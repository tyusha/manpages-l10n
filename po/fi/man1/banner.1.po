# Finnish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Tommi Vainikainen <mucus@pcuf.fi>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 18:41+0200\n"
"PO-Revision-Date: 1999-10-11 13:30+0200\n"
"Last-Translator: Tommi Vainikainen <mucus@pcuf.fi>\n"
"Language-Team: Finnish <>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.2\n"

#. type: TH
#: archlinux
#, no-wrap
msgid "BANNER"
msgstr "BANNER"

#. type: TH
#: archlinux
#, no-wrap
msgid "28 August 1996"
msgstr "28. elokuuta 1996"

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NIMI"

#. type: Plain text
#: archlinux
msgid "banner - print large letters"
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "YLEISKATSAUS"

#. type: Plain text
#: archlinux
msgid ""
"B<banner> [-Chlv] [-c character] [-f font] [-w cols] [--help] [--use-"
"letters] [--version] [--use-char=character] [--font=font] [--width=cols] "
"[text...]"
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "KUVAUS"

#. type: Plain text
#: archlinux
msgid ""
"The B<banner> program is used to print large letters of a given text. If the "
"text is not given on the command line, it will be read from stdin."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"This implementation of banner splits the text in multiple lines if it is too "
"long to fit on a single line. Splitting is done on word-boundaries, if "
"possible. To force a line break, include a newline characters in the text."
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Note that contigous whitespace characters are folded to a single spaace."
msgstr ""

#. type: SS
#: archlinux
#, no-wrap
msgid "OPTIONS"
msgstr "VALITSIMET"

#. type: TP
#: archlinux
#, no-wrap
msgid "I<-C, --center>"
msgstr "I<-C, --center>"

#. type: Plain text
#: archlinux
msgid "Center each line of the message."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "I<-h, --help>"
msgstr "I<-h, --help>"

#. type: Plain text
#: archlinux
msgid "Print a usage message on standard output and exit successfully."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "I<-l, --use-letters>"
msgstr "I<-l, --use-letters>"

#. type: Plain text
#: archlinux
msgid ""
"Use individual characters when drawing the letters, that is: Draw an `a' "
"using a-characters, a `b' using b-characters, and so on."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "I<-V, --version>"
msgstr "I<-V, --version>"

#. type: Plain text
#: archlinux
msgid "Print version information on standard output then exit successfully."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "I<-c, --use-char=character>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Use the given I<character> when drawing the letters. The default is an "
"asterisk (`*')."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "I<-f, --font=font>"
msgstr ""

#. type: Plain text
#: archlinux
msgid ""
"Select font number I<cols> for drawing characters. There are two available "
"fonts: Number 1 is the default font. Number 2 is a smaller font."
msgstr ""

#. type: TP
#: archlinux
#, no-wrap
msgid "I<-w, --width=cols>"
msgstr ""

#. type: Plain text
#: archlinux
msgid "Assume the screen is I<cols> columns wide. The default is 75."
msgstr ""

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "AUTHOR"
msgstr "TEKIJÄ"

#. type: Plain text
#: archlinux
msgid "Sverre H. Huseby (shh@thathost.com)."
msgstr "Sverre H. Huseby (shh@thathost.com)."

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "banner"
msgstr "banner"

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Oct 2020"
msgstr "Lokakuuta 2020"

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Banner"
msgstr "Banner"

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Kenneth J. Pronovici"
msgstr "Kenneth J. Pronovici"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid "banner - prints a short string to the console in very large letters"
msgstr ""

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid "B<banner> I<string>"
msgstr ""

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"This is a classic-style banner program similar to the one found in Solaris "
"or AIX in the late 1990s.  It prints a short string to the console in very "
"large letters."
msgstr ""

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"Banners that do not fit in the terminal will be truncated.  If $COLUMNS is "
"exported in the environment, it is taken to be the width of the terminal.  "
"If $COLUMNS is not exported, and TIOCGWINSZ is available on the platform, "
"then its idea of the terminal size is used.  Otherwise, a terminal width of "
"80 characters is assumed."
msgstr ""

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"Usage is straightforward.  For instance, a single word is printed like this:"
msgstr ""

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "   E<gt> banner ken\n"
msgstr "   E<gt> banner ken\n"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"   #    #  #######  #     #\n"
"   #   #   #        ##    #\n"
"   #  #    #        # #   #\n"
"   ###     #####    #  #  #\n"
"   #  #    #        #   # #\n"
"   #   #   #        #    ##\n"
"   #    #  #######  #     #\n"
msgstr ""
"   #    #  #######  #     #\n"
"   #   #   #        ##    #\n"
"   #  #    #        # #   #\n"
"   ###     #####    #  #  #\n"
"   #  #    #        #   # #\n"
"   #   #   #        #    ##\n"
"   #    #  #######  #     #\n"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid "Multiple arguments are printed on separate lines:"
msgstr ""

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "   E<gt> banner one two  \n"
msgstr "   E<gt> banner one two  \n"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"   #######  #     #  #######  \n"
"   #     #  ##    #  #        \n"
"   #     #  # #   #  #        \n"
"   #     #  #  #  #  #####    \n"
"   #     #  #   # #  #        \n"
"   #     #  #    ##  #        \n"
"   #######  #     #  #######  \n"
msgstr ""
"   #######  #     #  #######  \n"
"   #     #  ##    #  #        \n"
"   #     #  # #   #  #        \n"
"   #     #  #  #  #  #####    \n"
"   #     #  #   # #  #        \n"
"   #     #  #    ##  #        \n"
"   #######  #     #  #######  \n"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"   #######  #     #  #######  \n"
"      #     #  #  #  #     #  \n"
"      #     #  #  #  #     #  \n"
"      #     #  #  #  #     #  \n"
"      #     #  #  #  #     #  \n"
"      #     #  #  #  #     #  \n"
"      #      ## ##   ####### \n"
msgstr ""
"   #######  #     #  #######  \n"
"      #     #  #  #  #     #  \n"
"      #     #  #  #  #     #  \n"
"      #     #  #  #  #     #  \n"
"      #     #  #  #  #     #  \n"
"      #     #  #  #  #     #  \n"
"      #      ## ##   ####### \n"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"To get a single long line containing whitespace, you must quote the string:"
msgstr ""

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "   E<gt> banner \"one two\"\n"
msgstr "   E<gt> banner \"one two\"\n"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid ""
"   #######  #     #  #######        #######  #     #  #######  \n"
"   #     #  ##    #  #                 #     #  #  #  #     #  \n"
"   #     #  # #   #  #                 #     #  #  #  #     #  \n"
"   #     #  #  #  #  #####             #     #  #  #  #     #  \n"
"   #     #  #   # #  #                 #     #  #  #  #     #  \n"
"   #     #  #    ##  #                 #     #  #  #  #     #  \n"
"   #######  #     #  #######           #      ## ##   #######  \n"
msgstr ""
"   #######  #     #  #######        #######  #     #  #######  \n"
"   #     #  ##    #  #                 #     #  #  #  #     #  \n"
"   #     #  # #   #  #                 #     #  #  #  #     #  \n"
"   #     #  #  #  #  #####             #     #  #  #  #     #  \n"
"   #     #  #   # #  #                 #     #  #  #  #     #  \n"
"   #     #  #    ##  #                 #     #  #  #  #     #  \n"
"   #######  #     #  #######           #      ## ##   #######  \n"

#. type: SH
#: fedora-37 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "AVAILABILITY"
msgid "COMPATIBILITY"
msgstr "SAATAVUUS"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"From time to time, people assert that this program is buggy because it "
"doesn't do something that some other banner implementation does.  The "
"behavior of the program is based on what I saw on Solaris and AIX systems at "
"the time I wrote it in the late 1990s.  I make no claims that the behavior "
"is identical to that of any other contemporary system, especially any non-"
"free system that I may or may not have access to."
msgstr ""

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"If you don't like the behavior, you can either submit a patch, or you can "
"use an alternative program such as figlet.  I am always happy to accept "
"patches, and I promise to integrate patches promptly if provided.  So far, "
"no one who's complained has bothered to provide any patches, so the behavior "
"remains the same."
msgstr ""

#. type: SH
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "BUGS"
msgstr "BUGIT"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid "Report bugs to E<lt>support@cedar-solutions.comE<gt>."
msgstr ""
"Ilmoita ohjelmistovioista (englanniksi) osoitteeseen E<lt>support@cedar-"
"solutions.comE<gt>."

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid "Written by Kenneth J. Pronovici E<lt>pronovic@ieee.orgE<gt>."
msgstr "Kirjoittanut Kenneth J. Pronovici E<lt>pronovic@ieee.orgE<gt>."

#. type: SH
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "COPYRIGHT"
msgstr "TEKIJÄNOIKEUDET"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid "Copyright (c) 2000-2004.2007,2013,2014 Kenneth J. Pronovici."
msgstr "Copyright (c) 2000-2004.2007,2013,2014 Kenneth J. Pronovici."

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"This is free software; see the source for copying conditions.  There is NO "
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
