# Czech translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Stinovlas <stinovlas@gmail.com>, 2009.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-03 15:49+0200\n"
"PO-Revision-Date: 2009-09-02 20:06+0100\n"
"Last-Translator: Stinovlas <stinovlas@gmail.com>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.08.2\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "PWDX"
msgstr "PWDX"

#. type: TH
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid "July 2014"
msgid "June 2011"
msgstr "Července 2014"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr "Příručka uživatele"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "JMÉNO"

#. type: Plain text
#: opensuse-leap-15-5
msgid "pwdx - report current working directory of a process"
msgstr "pwdx - zjišťuje aktuální pracovní adresář procesu"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "POUŽITÍ"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<pwdx> [I<options>] I<pid> [...]"
msgstr "B<pwdx> [I<volby>] I<pid> [...]"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "VOLBY"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Output version information and exit."
msgstr "Vypíše číslo verze na standardní výstup a bezchybně skončí."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid "output version information and exit"
msgid "Output help screen and exit."
msgstr "Vypíše číslo verze na standardní výstup a bezchybně skončí."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "DALŠÍ INFORMACE"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<ps>(1), B<pgrep>(1)"
msgstr "B<ps>(1), B<pgrep>(1)"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDY"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "No standards apply, but pwdx looks an awful lot like a SunOS command."
msgid "No standards apply, but pwdx looks an awful lot like a SunOS command."
msgstr ""
"Nevstahují se na něj žádné standardy, ale pwdx až příliš vypadá jako příkaz "
"ze SunOS."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: opensuse-leap-15-5
msgid "E<.UR nmiell@gmail.com> Nicholas Miell E<.UE> wrote pwdx in 2004."
msgstr "E<.UR nmiell@gmail.com> Nicholas Miell E<.UE> napsal pwdx v roce 2004."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "HLÁŠENÍ CHYB"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Chyby týkající se programu prosím zasílejte na E<.UR procps@freelists.org> "
"E<.UE>"
