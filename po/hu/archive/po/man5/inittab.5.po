# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Szalay Attila <sasa@sophia.jpte.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-08-27 16:52+0200\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Szalay Attila <sasa@sophia.jpte.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "INITTAB"
msgstr "INITTAB"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Dec 4, 2001"
msgstr ""

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "Linux System Administrator's Manual"
msgstr ""

#. }}}
#. {{{  Name
#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. }}}
#. {{{  Description
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"inittab - format of the inittab file used by the sysv-compatible init process"
msgstr ""
"inittab - a sysv-compatibilis init process által használt inittab fájl "
"formátum."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The B<inittab> file describes which processes are started at bootup and "
"during normal operation (e.g.\\& /etc/init.d/boot, /etc/init.d/rc, "
"gettys...).  B<Init>(8)  distinguishes multiple I<runlevels>, each of which "
"can have its own set of processes that are started.  Valid runlevels are "
"B<0>-B<6> plus B<A>, B<B>, and B<C> for B<ondemand> entries.  An entry in "
"the B<inittab> file has the following format:"
msgstr ""
"Az B<inittab> fájl leírja mely processek indulnak el az induláskor és a "
"normál műveletek közben (például:\\& /etc/rc, getty-k ...). Az B<init>(8)  "
"megkülönböztet olyan többszörös I<futásszinteket>, ezek mindegyike saját "
"beállítása lehet a futtantandó programokról. Érvényés futási szintek a B<0>-"
"B<6>, ezenkívűl B<A>, B<B>, és B<C> a B<helybeni> bejegyzésekhez. Egy "
"inittab fájlbejegyzésnek a következő formátumúnak kell lennie:"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "I<id>:I<runlevels>:I<action>:I<process>"
msgstr "I<azon.>:I<futásszint>:I<tevékenység>:I<processz>"

#. {{{  id
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Lines beginning with `#' are ignored."
msgstr "A `#'-el kezdödő sorok mellőzésre kerülnek."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "I<id>"
msgstr "I<id>"

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
#| msgid ""
#| "is a unique sequence of 1-4 characters which identifies an entry in "
#| "B<inittab> (for versions of sysvinit compiled with libraries E<lt> 5.2.18 "
#| "or a.out libraries the limit is 2 characters)."
msgid ""
"is a unique sequence of 1-4 characters which identifies an entry in "
"B<inittab> (for versions of sysvinit compiled with the I<old> libc5 (E<lt> "
"5.2.18) or a.out libraries the limit is 2 characters)."
msgstr ""
"Egyedi, 1-4 elembol allo karakterlanc, ami azonosítja a bejegyzést az "
"inittabban (azon sysvinit verziók esetében amelyek fordításához tartozó "
"library E<lt> 5.2.18 , vagy még az a.out-os rendszerhez készűlt, a korlát 2 "
"karakter)."

#. }}}
#. {{{  runlevels
#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
#| msgid ""
#| "Note: For gettys or other login processes, the I<id> field should be the "
#| "tty suffix of the corresponding tty, e.g.\\& B<1> for B<tty1>.  "
#| "Otherwise, the login accounting might not work correctly."
msgid ""
"Note: traditionally, for getty and other login processes, the value of the "
"I<id> field is kept the same as the suffix of the corresponding tty, e.g.\\& "
"B<1> for B<tty1>. Some ancient login accounting programs might expect this, "
"though I can't think of any."
msgstr ""
"Megjegyzés: Getty vagy másmilyen login processesknél az I<azon> mező a tty "
"megfelelő tty rangjának kell lennie, például\\& B<1> a B<tty1> helyett. "
"Másképpen a bejelentkezési nyilvántartás helytelenűl működhet."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "I<runlevels>"
msgstr "I<futásszintek>"

#. }}}
#. {{{  action
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "lists the runlevels for which the specified action should be taken."
msgstr "Leirják, hogy melyik futási szinteken jön létre az adott tevékenység."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "I<action>"
msgstr "I<tevékenység>"

#. }}}
#. {{{  process
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "describes which action should be taken."
msgstr "leírja, hogy milyen tevékenység jöjjön létre."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "I<process>"
msgstr "I<process>"

#. }}}
#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy
#| msgid ""
#| "specifies the process to be executed.  If the process field starts with a "
#| "`+' character, B<init> will not do utmp and wtmp accounting for that "
#| "process.  This is needed for gettys that insist on doing their own utmp/"
#| "wtmp housekeeping.  This is also a historic bug."
msgid ""
"specifies the process to be executed.  If the process field starts with a `"
"+' character, B<init> will not do utmp and wtmp accounting for that "
"process.  This is needed for gettys that insist on doing their own utmp/wtmp "
"housekeeping.  This is also a historic bug. The length of this field is "
"limited to 127 characters."
msgstr ""
"Meghatározza, hogy melyik process indítodjon el. Ha a process mező `+' "
"karakterrel kezdödik, akkor az init nem csinál utmp és wtmp nyilvántartást a "
"processnek. Ez akkor szükséges, ha a getty ragaszkodik a saját utmp/wtmp "
"háztartásához. Ez egy történelmi hiba."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The I<runlevels> field may contain multiple characters for different "
"runlevels.  For example, B<123> specifies that the process should be started "
"in runlevels 1, 2, and 3.  The I<runlevels> for B<ondemand> entries may "
"contain an B<A>, B<B>, or B<C>.  The I<runlevels> field of B<sysinit>, "
"B<boot>, and B<bootwait> entries are ignored."
msgstr ""
"A I<futásszintek> mező tőbb értéket is tartalmazhat, a különböző "
"futásszintekhez. Például az B<123> meghatározza, hogy a programnak az 1-es, "
"2-es és 3-as futásszinten kell futnia.  Az B<helybeni> futásszint "
"bejegyzések lehetnek B<A>, B<B>, vagy B<C>. A B<sysinit>, B<boot>, és "
"B<bootwait> futásszint bejegyzések figyelmen kívűl lesznek hagyva."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"When the system runlevel is changed, any running processes that are not "
"specified for the new runlevel are killed, first with \\s-2SIGTERM\\s0, then "
"with \\s-2SIGKILL\\s0."
msgstr ""
"Ha a rendszer futásszintje megváltozik, az összes olyan program, ami az új "
"futásszinthez nincs bejegyezve, le lesz álítva, előszőr a \\s-2SIGTERM\\s0, "
"majd a \\s-2SIGKILL\\s0 jelzéssel."

#. {{{  respawn
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Valid actions for the I<action> field are:"
msgstr "Érvényes tevékenységek a I<tevékenység> mezőhöz:"

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<respawn>"
msgstr "B<respawn>"

#. }}}
#. {{{  wait
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "The process will be restarted whenever it terminates (e.g.\\& getty)."
msgstr "A process újraindul valahányszor megszakítódik (pl:\\& getty)."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<wait>"
msgstr "B<wait>"

#. }}}
#. {{{  once
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The process will be started once when the specified runlevel is entered and "
"B<init> will wait for its termination."
msgstr ""
"A process akkor indul el, amikor a megadott futási szintre lép, és az "
"B<init> addig vár, amíg a processz fut.."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<once>"
msgstr "B<once>"

#. }}}
#. {{{  boot
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The process will be executed once when the specified runlevel is entered."
msgstr "A processzt egyszer, a futásszintre lépéskor indítja el."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<boot>"
msgstr "B<boot>"

#. }}}
#. {{{  bootwait
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The process will be executed during system boot.  The I<runlevels> field is "
"ignored."
msgstr ""
"A process a rendszer újrainditása közben indul el. Ilyenkor a I<futásszint> "
"mező tartalma nem érdekes."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<bootwait>"
msgstr "B<bootwait>"

#. }}}
#. {{{  off
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The process will be executed during system boot, while B<init> waits for its "
"termination (e.g.\\& /etc/rc).  The I<runlevels> field is ignored."
msgstr ""
"A processzt a rendszer indulása közben indítja el, az B<init> megvárja míg "
"lefut (például:\\& /etc/rc). A I<futásszint> mező mellőzésre kerül."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<off>"
msgstr "B<off>"

#. }}}
#. {{{  ondemand
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "This does nothing."
msgstr "Semmit sem csinál."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<ondemand>"
msgstr "B<ondemand>"

#. }}}
#. {{{  initdefault
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"A process marked with an B<ondemand> runlevel will be executed whenever the "
"specified B<ondemand> runlevel is called.  However, no runlevel change will "
"occur (B<ondemand> runlevels are `a', `b', and `c')."
msgstr ""
"Az B<ondemand> (helybeni) futásszinttel megjelölt process elindul, "
"valahányszor a megadott helybeni futásszint meghivódik.  Viszont nem "
"következik be futásszint csere(B<helybeni> futási szintek az `a', a `b' és a "
"`c')."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<initdefault>"
msgstr "B<initdefault>"

#. }}}
#. {{{  sysinit
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"An B<initdefault> entry specifies the runlevel which should be entered after "
"system boot.  If none exists, B<init> will ask for a runlevel on the "
"console. The I<process> field is ignored."
msgstr ""
"Az B<initdefault> bejegyzés megadja, hogy melyik futási szintre lépünk be a "
"rendszer újraindítása után. Ha ilyent nem adtunk meg, akkor az B<init> a "
"konzolról kér be egy futásszintet. A I<processz> mező ilyenkor figyelmen "
"kívűl hagyódik."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<sysinit>"
msgstr "B<sysinit>"

#. }}}
#. {{{  powerwait
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The process will be executed during system boot. It will be executed before "
"any B<boot> or B< bootwait> entries.  The I<runlevels> field is ignored."
msgstr ""
"A process a rendszer újraindítása alatt hajtódik végre, mégpedig minden "
"B<boot> és B<bootwait> bejegyzés elött. A I<futásszint> mező tartalma "
"lényegtelen."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<powerwait>"
msgstr "B<powerwait>"

#. }}}
#. {{{  powerfail
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The process will be executed when the power goes down. Init is usually "
"informed about this by a process talking to a UPS connected to the "
"computer.  B<Init> will wait for the process to finish before continuing."
msgstr ""
"A processz akkor lesz végrehajtva, ha megszakad az áramszolgáltatás. Az init "
"erről általában egy olyan processztől értesűl, ami egy UPS-sel (szünetmentes "
"áramforrás) komunikál. Az B<init> ilyenkor megvárja, hogy a processz "
"befelyezödjön mielött továbbmenne."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<powerfail>"
msgstr "B<powerfail>"

#. }}}
#. {{{  powerokwait
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"As for B<powerwait>, except that B<init> does not wait for the process's "
"completion."
msgstr ""
"Ugyanaz, mint a B<powerwait>, kivéve, hogy az B<init> ilyenkor nem várja meg "
"a processz befejeződését."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<powerokwait>"
msgstr "B<powerokwait>"

#. }}}
#. {{{  powerfailnow
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"This process will be executed as soon as B<init> is informed that the power "
"has been restored."
msgstr ""
"Ez a processz azonnal végre lesz hajtva, amint az B<init> arról értesűl, "
"hogy az áram visszatért."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<powerfailnow>"
msgstr "B<powerfailnow>"

#. }}}
#. {{{  ctrlaltdel
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"This process will be executed when B<init> is told that the battery of the "
"external UPS is almost empty and the power is failing (provided that the "
"external UPS and the monitoring process are able to detect this condition)."
msgstr ""
"Ez a processz akkor lesz vegrehajtva, ha azt közlik az init-el, hogy a külső "
"UPS elemei majdnem teljesen üresek, és az áramszolgáltatás megszünt "
"(feltételezi, hogy a külső UPS és az ellenőrző program képes ezt az "
"állapotot érzékelni)."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<ctrlaltdel>"
msgstr "B<ctrlaltdel>"

#. }}}
#. {{{  kbrequest
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The process will be executed when B<init> receives the SIGINT signal.  This "
"means that someone on the system console has pressed the B<CTRL-ALT-DEL> key "
"combination. Typically one wants to execute some sort of B<shutdown> either "
"to get into single-user level or to reboot the machine."
msgstr ""
"A processz akkor lesz végrehajtva, ha az B<init> egy SIGINT szignált kap. Ez "
"azt jelenti, hogy valaki a rendszer konzolján lenyomta a B<CTRL-ALT-DEL> "
"billentyű kombinációt. Általában ez azt jelenti, hogy valaki valamiféle "
"B<shutdown>-t akar végrehajtani: vagy egyfelhasználós szintre akar eljutni, "
"vagy pedig újra akarja indítani a gépet."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<kbrequest>"
msgstr "B<kbrequest>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The process will be executed when B<init> receives a signal from the "
"keyboard handler that a special key combination was pressed on the console "
"keyboard."
msgstr ""
"A process akkor lesz végrehajtva, ha az B<init> egy szignált kap a "
"billentyűzet kezelötöl, ami azt jelzi, hogy egy speciális billentyű "
"kombináció lett lenyomva a konzol billentyűzetén."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"The documentation for this function is not complete yet; more documentation "
"can be found in the kbd-x.xx packages (most recent was kbd-0.94 at the time "
"of this writing). Basically you want to map some keyboard combination to the "
"\"KeyboardSignal\" action. For example, to map Alt-Uparrow for this purpose "
"use the following in your keymaps file:"
msgstr ""
"Ennek a funkciónak a leírása még nem teljes; további dokumentációt a kbd-x."
"xx csomagokban lehet találni (a legújabb a kbd-0.94 csomag volt a "
"dokumentácó írása idején). Valószínüleg néhány billentyűzet kombinációt akar "
"feltérképezni a \"KeyboardSignal\" akcióhoz. Például, hogy az Alt-"
"felfelényíl kombinációt e célból feltérképezze, használja a következöt a "
"keymaps fájljában:"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "alt keycode 103 = KeyboardSignal"
msgstr "alt keycode 103 = KeyboardSignal"

#. }}}
#. }}}
#. {{{  Examples
#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "EXAMPLES"
msgstr "PÉLDÁK"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "This is an example of a inittab which resembles the old Linux inittab:"
msgstr "Ez egy példa az inittab-ra, ami emlékeztet a régi Linux-os inittab-ra:"

#. type: Plain text
#: debian-bullseye debian-unstable
#, no-wrap
msgid ""
"# inittab for linux\n"
"id:1:initdefault:\n"
"rc::bootwait:/etc/rc\n"
"1:1:respawn:/etc/getty 9600 tty1\n"
"2:1:respawn:/etc/getty 9600 tty2\n"
"3:1:respawn:/etc/getty 9600 tty3\n"
"4:1:respawn:/etc/getty 9600 tty4\n"
msgstr ""
"# inittab linux-ra\n"
"id:1:initdefault:\n"
"rc::bootwait:/etc/rc\n"
"1:1:respawn:/etc/getty 9600 tty1\n"
"2:1:respawn:/etc/getty 9600 tty2\n"
"3:1:respawn:/etc/getty 9600 tty3\n"
"4:1:respawn:/etc/getty 9600 tty4\n"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"This inittab file executes B</etc/rc> during boot and starts gettys on tty1-"
"tty4."
msgstr ""
"Ez az inittab végrehajtja az B</etc/rc> paracsfájlt a boot-folyamat alatt és "
"elindítja a gettys-t a tty1-tty4-en."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"A more elaborate B<inittab> with different runlevels (see the comments "
"inside):"
msgstr ""
"Egy bonyolultabb inittab különbözö futási szintekkel (Lásd a kommenteket a "
"fájl-on belül):"

#. type: Plain text
#: debian-bullseye debian-unstable
#, no-wrap
msgid ""
"# Level to run in\n"
"id:2:initdefault:\n"
msgstr ""
"# Szint amin fussunk\n"
"id:2:initdefault:\n"

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy, no-wrap
#| msgid ""
#| "# System initialization before anything else.\n"
#| "si::sysinit:/etc/rc.d/bcheckrc\n"
msgid ""
"# Boot-time system configuration/initialization script.\n"
"si::sysinit:/etc/init.d/rcS\n"
msgstr ""
"# Rendszer inicializáció minden más elött.\n"
"si::sysinit:/etc/rc.d/bcheckrc\n"

#. type: Plain text
#: debian-bullseye debian-unstable
#, no-wrap
msgid ""
"# What to do in single-user mode.\n"
"~:S:wait:/sbin/sulogin\n"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
#, no-wrap
msgid ""
"# /etc/init.d executes the S and K scripts upon change\n"
"# of runlevel.\n"
"#\n"
"# Runlevel 0 is halt.\n"
"# Runlevel 1 is single-user.\n"
"# Runlevels 2-5 are multi-user.\n"
"# Runlevel 6 is reboot.\n"
msgstr ""

#. type: Plain text
#: debian-bullseye debian-unstable
#, no-wrap
msgid ""
"l0:0:wait:/etc/init.d/rc 0\n"
"l1:1:wait:/etc/init.d/rc 1\n"
"l2:2:wait:/etc/init.d/rc 2\n"
"l3:3:wait:/etc/init.d/rc 3\n"
"l4:4:wait:/etc/init.d/rc 4\n"
"l5:5:wait:/etc/init.d/rc 5\n"
"l6:6:wait:/etc/init.d/rc 6\n"
msgstr ""
"l0:0:wait:/etc/init.d/rc 0\n"
"l1:1:wait:/etc/init.d/rc 1\n"
"l2:2:wait:/etc/init.d/rc 2\n"
"l3:3:wait:/etc/init.d/rc 3\n"
"l4:4:wait:/etc/init.d/rc 4\n"
"l5:5:wait:/etc/init.d/rc 5\n"
"l6:6:wait:/etc/init.d/rc 6\n"

#. type: Plain text
#: debian-bullseye debian-unstable
#, no-wrap
msgid ""
"# What to do at the \"3 finger salute\".\n"
"ca::ctrlaltdel:/sbin/shutdown -t1 -h now\n"
msgstr ""
"# Mit csináljunk a \"3 ujjas tisztelgés\"-nél.\n"
"ca::ctrlaltdel:/sbin/shutdown -t1 -h now\n"

#. type: Plain text
#: debian-bullseye debian-unstable
#, fuzzy, no-wrap
#| msgid ""
#| "# Runlevel 2&3: getty on console, level 3 also getty on modem port.\n"
#| "1:23:respawn:/sbin/getty tty1 VC linux\n"
#| "2:23:respawn:/sbin/getty tty2 VC linux\n"
#| "3:23:respawn:/sbin/getty tty3 VC linux\n"
#| "4:23:respawn:/sbin/getty tty4 VC linux\n"
#| "S2:3:respawn:/sbin/uugetty ttyS2 M19200\n"
msgid ""
"# Runlevel 2,3: getty on virtual consoles\n"
"# Runlevel   3: getty on terminal (ttyS0) and modem (ttyS1)\n"
"1:23:respawn:/sbin/getty tty1 VC linux\n"
"2:23:respawn:/sbin/getty tty2 VC linux\n"
"3:23:respawn:/sbin/getty tty3 VC linux\n"
"4:23:respawn:/sbin/getty tty4 VC linux\n"
"S0:3:respawn:/sbin/getty -L 9600 ttyS0 vt320\n"
"S1:3:respawn:/sbin/mgetty -x0 -D ttyS1\n"
msgstr ""
"# Futásszint 2&3: getty konzolon, 3-as szinten a modemhez is.\n"
"1:23:respawn:/sbin/getty tty1 VC linux\n"
"2:23:respawn:/sbin/getty tty2 VC linux\n"
"3:23:respawn:/sbin/getty tty3 VC linux\n"
"4:23:respawn:/sbin/getty tty4 VC linux\n"
"S2:3:respawn:/sbin/uugetty ttyS2 M19200\n"

#. }}}
#. {{{  Files
#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "FILES"
msgstr "FÁJLOK"

#. }}}
#. {{{  Author
#. type: Plain text
#: debian-bullseye debian-unstable
msgid "/etc/inittab"
msgstr "/etc/inittab"

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "AUTHOR"
msgstr "SZERZŐ"

#. }}}
#. {{{  See also
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<Init> was written by Miquel van Smoorenburg (miquels@cistron.nl).  This "
"manual page was written by Sebastian Lederer (lederer@francium.informatik."
"uni-bonn.de) and modified by Michael Haardt (u31b3hs@pool.informatik.rwth-"
"aachen.de)."
msgstr ""
"A B<init>-et Miquel van Smoorenburg (miquels@cistron.nl) , a hozzávaló "
"kézikönyvlapot pedig Sebastian Lederer (lederer@francium.informatik.uni-bonn."
"de) írta és Michael Haardt (u31b3hs@pool.informatik.rwth-aachen.de) "
"módosította."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "B<init>(8), B<telinit>(8)"
msgstr "B<init>(8), B<telinit>(8)"
