# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2014, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2022-10-17 20:33+0200\n"
"PO-Revision-Date: 2022-02-15 20:41+0100\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "LSMOD"
msgstr "LSMOD"

#. type: TH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "08/17/2021"
msgid "06/30/2022"
msgstr "17.08.2021 r."

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "kmod"
msgstr "kmod"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "lsmod"
msgstr "lsmod"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "lsmod - Show the status of modules in the Linux Kernel"
msgstr "lsmod - pokazuje stan modułów w jądrze Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<lsmod>"
msgstr "B<lsmod>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<lsmod> is a trivial program which nicely formats the contents of the /proc/"
"modules, showing what kernel modules are currently loaded\\&."
msgstr ""
"B<lsmod> jest trywialnym programem, który ładnie formatuje zawartość pliku /"
"proc/modules, pokazując aktualnie załadowane moduły jądra\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "PRAWA AUTORSKIE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This manual page originally Copyright 2002, Rusty Russell, IBM "
"Corporation\\&. Maintained by Jon Masters and others\\&."
msgstr ""
"Pierwotnie: Copyright 2002, Rusty Russell, IBM Corporation\\&. Strona "
"podręcznika jest obecnie zarządzana przez Jona Mastersa i innych\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<insmod>(8), B<modprobe>(8), B<modinfo>(8)  B<depmod>(8)"
msgstr "B<insmod>(8), B<modprobe>(8), B<modinfo>(8)  B<depmod>(8)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORZY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<Jon Masters> E<lt>\\&jcm@jonmasters\\&.org\\&E<gt>"
msgstr "B<Jon Masters> E<lt>\\&jcm@jonmasters\\&.org\\&E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Developer"
msgstr "Deweloper"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<Lucas De Marchi> E<lt>\\&lucas\\&.de\\&.marchi@gmail\\&.com\\&E<gt>"
msgstr "B<Lucas De Marchi> E<lt>\\&lucas\\&.de\\&.marchi@gmail\\&.com\\&E<gt>"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "01/08/2021"
msgstr "08.01.2021 r."

#. type: TH
#: debian-unstable
#, fuzzy, no-wrap
#| msgid "01/08/2021"
msgid "10/08/2022"
msgstr "08.01.2021 r."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "01/29/2021"
msgstr "29.01.2021 r."
