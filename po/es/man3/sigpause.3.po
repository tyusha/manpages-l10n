# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-17 20:49+0200\n"
"PO-Revision-Date: 1998-01-30 19:53+0200\n"
"Last-Translator: Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SIGPAUSE"
msgstr "SIGPAUSE"

#. type: TH
#: archlinux
#, no-wrap
msgid "2022-09-09"
msgstr "9 Septiembre 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de manual de Linux (no publicado)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "sigpause - atomically release blocked signals and wait for interrupt"
msgstr ""
"sigpause - libera atómicamente señales bloqueadas y espera interrupción"

#. type: SH
#: archlinux
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>signal.hE<gt>>\n"
msgstr "B<#include E<lt>signal.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<int sigpause(int >I<sigmask>B<);>"
msgid "B<int sigpause(int >I<sigmask>B<);  /* BSD (but see NOTES) */>\n"
msgstr "B<int sigpause(int >I<sigmask>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<int sigpause(int >I<sig>B<);      /* System V / UNIX 95 */>\n"
msgstr "B<int sigpause(int >I<sig>B<);      /* System V / UNIX 95 */>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Don't use this function.  Use B<sigsuspend>(2)  instead."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<sigpause> assigns I<sigmask> to the set of masked signals and then "
#| "waits for a signal to arrive; on return the set of masked signals is "
#| "restored."
msgid ""
"The function B<sigpause>()  is designed to wait for some signal.  It changes "
"the process's signal mask (set of blocked signals), and then waits for a "
"signal to arrive.  Upon arrival of a signal, the original signal mask is "
"restored."
msgstr ""
"B<sigpause> asigna I<sigmask> al conjunto de señales enmascaradas y luego "
"espera que llegue una señal; al regresar, se restaura el conjunto de señales "
"enmascaradas."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALOR DEVUELTO"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If B<sigpause>()  returns, it was interrupted by a signal and the return "
"value is -1 with I<errno> set to B<EINTR>."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para obtener una explicación de los términos usados en esta sección, véase "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interfaz"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<sigpause>()"
msgstr "B<sigpause>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Seguridad del hilo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "Multi-hilo seguro"

#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#: archlinux
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The System V version of B<sigpause>()  is standardized in POSIX.1-2001.  It "
"is also specified in POSIX.1-2008, where it is marked obsolete."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "History"
msgstr "Historia"

#.  __xpg_sigpause: UNIX 95, spec 1170, SVID, SVr4, XPG
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The classical BSD version of this function appeared in 4.2BSD.  It sets the "
"process's signal mask to I<sigmask>.  UNIX 95 standardized the incompatible "
"System V version of this function, which removes only the specified signal "
"I<sig> from the process's signal mask.  The unfortunate situation with two "
"incompatible functions with the same name was solved by the B<\\"
"%sigsuspend>(2)  function, that takes a I<sigset_t\\ *> argument (instead of "
"an I<int>)."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "Linux"
msgid "Linux notes"
msgstr "Linux"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"On Linux, this routine is a system call only on the Sparc (sparc64)  "
"architecture."
msgstr ""

#.  Libc4 and libc5 know only about the BSD version.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Glibc uses the BSD version if the B<_BSD_SOURCE> feature test macro is "
"defined and none of B<_POSIX_SOURCE>, B<_POSIX_C_SOURCE>, B<_XOPEN_SOURCE>, "
"B<_GNU_SOURCE>, or B<_SVID_SOURCE> is defined.  Otherwise, the System V "
"version is used, and feature test macros must be defined as follows to "
"obtain the declaration:"
msgstr ""

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "*"
msgstr "*"

#.  || (_XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED)
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Since glibc 2.26: _XOPEN_SOURCE E<gt>= 500"
msgstr "Desde glibc 2.26: _XOPEN_SOURCE E<gt>= 500"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Glibc 2.25 and earlier: _XOPEN_SOURCE"
msgstr ""

#
#.  For the BSD version, one usually uses a zero
#.  .I sigmask
#.  to indicate that no signals are to be blocked.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Since glibc 2.19, only the System V version is exposed by I<E<lt>signal."
"hE<gt>>; applications that formerly used the BSD B<sigpause>()  should be "
"amended to use B<sigsuspend>(2)."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<kill>(2), B<sigaction>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<sigblock>(3), B<sigvec>(3), B<feature_test_macros>(7)"
msgstr ""
"B<kill>(2), B<sigaction>(2), B<sigprocmask>(2), B<sigsuspend>(2), "
"B<sigblock>(3), B<sigvec>(3), B<feature_test_macros>(7)"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#.  FIXME: The marking is different from that in the glibc manual,
#.  marking in glibc manual is more detailed:
#.  sigpause: MT-Unsafe race:sigprocmask/!bsd!linux
#.  glibc manual says /!linux!bsd indicate the preceding marker only applies
#.  when the underlying kernel is neither Linux nor a BSD kernel.
#.  So, it is safe in Linux kernel.
#. type: SH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.10 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 Marzo 2021"

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.13 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."
