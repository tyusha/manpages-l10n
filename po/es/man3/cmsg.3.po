# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Juan Piernas <piernas@ditec.um.es>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-17 20:19+0200\n"
"PO-Revision-Date: 2021-01-28 18:50+0100\n"
"Last-Translator: Juan Piernas <piernas@ditec.um.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "CMSG"
msgstr "CMSG"

#. type: TH
#: archlinux
#, no-wrap
msgid "2022-09-09"
msgstr "9 Septiembre 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Páginas de manual de Linux (no publicado)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"CMSG_ALIGN, CMSG_SPACE, CMSG_NXTHDR, CMSG_FIRSTHDR - access ancillary data"
msgstr ""
"CMSG_ALIGN, CMSG_SPACE, CMSG_NXTHDR, CMSG_FIRSTHDR - acceso a datos "
"auxiliares"

#. type: SH
#: archlinux
#, no-wrap
msgid "LIBRARY"
msgstr "BIBLIOTECA"

#. type: Plain text
#: archlinux
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr "Biblioteca Estándar C (I<libc>, I<-lc>)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr "B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<#include E<lt>sys/socket.hE<gt>>\n"
#| "B<struct cmsghdr *CMSG_FIRSTHDR(struct msghdr *>I<msgh>B<);>\n"
#| "B<struct cmsghdr *CMSG_NXTHDR(struct msghdr *>I<msgh>B<,>\n"
#| "B<                            struct cmsghdr *>cmsgB<);>\n"
#| "B<size_t CMSG_ALIGN(size_t >I<length>B<);>\n"
#| "B<size_t CMSG_SPACE(size_t >I<length>B<);>\n"
#| "B<size_t CMSG_LEN(size_t >I<length>B<);>\n"
#| "B<unsigned char *CMSG_DATA(struct cmsghdr *>I<cmsg>B<);>\n"
msgid ""
"B<struct cmsghdr *CMSG_FIRSTHDR(struct msghdr *>I<msgh>B<);>\n"
"B<struct cmsghdr *CMSG_NXTHDR(struct msghdr *>I<msgh>B<,>\n"
"B<                            struct cmsghdr *>cmsgB<);>\n"
"B<size_t CMSG_ALIGN(size_t >I<length>B<);>\n"
"B<size_t CMSG_SPACE(size_t >I<length>B<);>\n"
"B<size_t CMSG_LEN(size_t >I<length>B<);>\n"
"B<unsigned char *CMSG_DATA(struct cmsghdr *>I<cmsg>B<);>\n"
msgstr ""
"B<#include E<lt>sys/socket.hE<gt>>\n"
"B<struct cmsghdr *CMSG_FIRSTHDR(struct msghdr *>I<msgh>B<);>\n"
"B<struct cmsghdr *CMSG_NXTHDR(struct msghdr *>I<msgh>B<,>\n"
"B<                            struct cmsghdr *>cmsgB<);>\n"
"B<size_t CMSG_ALIGN(size_t >I<length>B<);>\n"
"B<size_t CMSG_SPACE(size_t >I<length>B<);>\n"
"B<size_t CMSG_LEN(size_t >I<length>B<);>\n"
"B<unsigned char *CMSG_DATA(struct cmsghdr *>I<cmsg>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"These macros are used to create and access control messages (also called "
"ancillary data) that are not a part of the socket payload.  This control "
"information may include the interface the packet was received on, various "
"rarely used header fields, an extended error description, a set of file "
"descriptors, or UNIX credentials.  For instance, control messages can be "
"used to send additional header fields such as IP options.  Ancillary data is "
"sent by calling B<sendmsg>(2)  and received by calling B<recvmsg>(2).  See "
"their manual pages for more information."
msgstr ""
"Estas macros se usan para crear y acceder a mensajes de control (también "
"llamados datos auxiliares) que no son parte del contenido útil de un "
"conector. Esta información de control puede incluir la interfaz en la que se "
"ha recibido el paquete, diferentes campos de cabecera usados raramente, una "
"descripción de error ampliada, un conjunto de descriptores de fichero o "
"credenciales de UNIX. Por ejemplo, los mensajes de control se pueden usar "
"para enviar campos de cabecera adicionales tales como opciones IP. Los datos "
"auxiliares se envían llamando a B<sendmsg>(2) y se reciben llamando a "
"B<recvmsg>(2). Vea sus páginas de manual para más información."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Ancillary data is a sequence of I<cmsghdr> structures with appended data.  "
"See the specific protocol man pages for the available control message "
"types.  The maximum ancillary buffer size allowed per socket can be set "
"using I</proc/sys/net/core/optmem_max>; see B<socket>(7)."
msgstr ""
"Los datos auxiliares son una secuencia de estructuras I<cmsghdr> con datos "
"añadidos. Vea las páginas de manual específicas del protocolo para conocer "
"los tipos de mensajes de control disponibles. El tamaño máximo permitido del "
"buffer auxiliar por conector se puede configura con I</proc/sys/net/core/"
"optmem_max>. Vea B<socket>(7)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "The I<cmsghdr> structure is defined as follows:"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "struct cmsghdr {\n"
#| "\tsocklen_t\tcmsg_len;\t/* data byte count, including header */\n"
#| "\tint\tcmsg_level;\t/* originating protocol */\n"
#| "\tint\tcmsg_type;\t/* protocol-specific type */\n"
#| "/* followed by  unsigned char\tcmsg_data[]; */\n"
#| "};\n"
msgid ""
"struct cmsghdr {\n"
"    size_t cmsg_len;    /* Data byte count, including header\n"
"                           (type is socklen_t in POSIX) */\n"
"    int    cmsg_level;  /* Originating protocol */\n"
"    int    cmsg_type;   /* Protocol-specific type */\n"
"/* followed by\n"
"   unsigned char cmsg_data[]; */\n"
"};\n"
msgstr ""
"struct cmsghdr {\n"
"\tsocklen_t\tcmsg_len;\t/* cantidad de bytes de datos, incluyendo la cabecera */\n"
"\tint\tcmsg_level;\t/* protocolo originario */\n"
"\tint\tcmsg_type;\t/* tipo específico del protocolo */\n"
"/* seguido de  unsigned char\tcmsg_data[]; */\n"
"};\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The sequence of I<cmsghdr> structures should never be accessed directly.  "
"Instead, use only the following macros:"
msgstr ""

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "*"
msgstr "*"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<CMSG_FIRSTHDR> returns a pointer to the first B<cmsghdr> in the "
#| "ancillary data buffer associated with the passed B<msghdr>."
msgid ""
"B<CMSG_FIRSTHDR>()  returns a pointer to the first I<cmsghdr> in the "
"ancillary data buffer associated with the passed I<msghdr>.  It returns NULL "
"if there isn't enough space for a I<cmsghdr> in the buffer."
msgstr ""
"B<CMSG_FIRSTHDR> devuelve un puntero a la primera B<cmsghdr> en el buffer de "
"datos auxiliares asociado con la B<msghdr> pasada."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<CMSG_NXTHDR>()  returns the next valid I<cmsghdr> after the passed "
"I<cmsghdr>.  It returns NULL when there isn't enough space left in the "
"buffer."
msgstr ""
"B<CMSG_NXTHDR>() devuelve la siguiente B<cmsghdr> válida después de la "
"I<cmsghdr> pasada. Devuelve NULL cuando no queda suficiente espacio en el "
"buffer."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When initializing a buffer that will contain a series of I<cmsghdr> "
"structures (e.g., to be sent with B<sendmsg>(2)), that buffer should first "
"be zero-initialized to ensure the correct operation of B<CMSG_NXTHDR>()."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<CMSG_ALIGN>(), given a length, returns it including the required "
"alignment.  This is a constant expression."
msgstr ""
"B<CMSG_ALIGN>(), dada una longitud, la devuelve incluyendo la alineación "
"necesaria. Ésta es una expresión constante."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<CMSG_SPACE>()  returns the number of bytes an ancillary element with "
"payload of the passed data length occupies.  This is a constant expression."
msgstr ""
"B<CMSG_SPACE>() devuelve la cantidad de bytes que ocupa un elemento auxiliar "
"cuyo contenido útil es de la longitud de datos pasada. Ésta es una expresión "
"constante."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<CMSG_DATA>()  returns a pointer to the data portion of a I<cmsghdr>.  The "
"pointer returned cannot be assumed to be suitably aligned for accessing "
"arbitrary payload data types.  Applications should not cast it to a pointer "
"type matching the payload, but should instead use B<memcpy>(3)  to copy data "
"to or from a suitably declared object."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<CMSG_LEN>()  returns the value to store in the I<cmsg_len> member of the "
"I<cmsghdr> structure, taking into account any necessary alignment.  It takes "
"the data length as an argument.  This is a constant expression."
msgstr ""
"B<CMSG_LEN>() devuelve el valor a almacenar en el miembro I<cmsg_len> de la "
"estructura I<cmsghdr> teniendo en cuenta cualquier alineación necesaria. "
"Toma como argumento la longitud de los datos. Ésta es una expresión "
"constante."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"To create ancillary data, first initialize the I<msg_controllen> member of "
"the I<msghdr> with the length of the control message buffer.  Use "
"B<CMSG_FIRSTHDR>()  on the I<msghdr> to get the first control message and "
"B<CMSG_NXTHDR>()  to get all subsequent ones.  In each control message, "
"initialize I<cmsg_len> (with B<CMSG_LEN>()), the other I<cmsghdr> header "
"fields, and the data portion using B<CMSG_DATA>().  Finally, the "
"I<msg_controllen> field of the I<msghdr> should be set to the sum of the "
"B<CMSG_SPACE>()  of the length of all control messages in the buffer.  For "
"more information on the I<msghdr>, see B<recvmsg>(2)."
msgstr ""
"Para crear datos auxiliares, inicialice primero el miembro I<msg_controllen> "
"de la estructura I<msghdr> con el tamaño del buffer de mensajes de control. "
"Use B<CMSG_FIRSTHDR>() sobre I<msghdr> para obtener el primer mensaje de "
"control y B<CMSG_NXTHDR>() para obtener los siguientes. En cada mensaje de "
"control, inicialice I<cmsg_len> (con B<CMSG_LEN>()), los otros campos "
"cabecera de I<cmsghdr> y la parte de datos usando B<CMSG_DATA>(). "
"Finalmente, debería asignar al campo I<msg_controllen> de I<msghdr> la suma "
"de los B<CMSG_SPACE>() de las longitudes de todos los mensajes de control "
"del buffer. Para más información sobre I<msghdr>, vea B<recvmsg>(2)."

#. type: SH
#: archlinux
#, no-wrap
msgid "STANDARDS"
msgstr "ESTÁNDARES"

#.  https://www.austingroupbugs.net/view.php?id=978#c3242
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "This ancillary data model conforms to the POSIX.1003.1g draft, 4.4BSD-"
#| "Lite, the IPv6 advanced API described in RFC2292 and the Single Unix "
#| "specification v2.  B<CMSG_ALIGN > is a Linux extension."
msgid ""
"This ancillary data model conforms to the POSIX.1g draft, 4.4BSD-Lite, the "
"IPv6 advanced API described in RFC\\ 2292 and SUSv2.  B<CMSG_FIRSTHDR>(), "
"B<CMSG_NXTHDR>(), and B<CMSG_DATA>()  are specified in POSIX.1-2008.  "
"B<CMSG_SPACE>()  and B<CMSG_LEN>()  will be included in the next POSIX "
"release (Issue 8)."
msgstr ""
"El modelo de datos auxiliares sigue el borrador POSIX.1003.1g, 4.4BSD-Lite, "
"la API avanzada de IPv6 descrita en RFC2292 y the Single Unix specification "
"v2. B<CMSG_ALIGN > es una extensión de Linux."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<CMSG_ALIGN>()  is a Linux extension."
msgstr "B<CMSG_ALIGN>() es una extensión de Linux."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For portability, ancillary data should be accessed using only the macros "
"described here.  B<CMSG_ALIGN>()  is a Linux extension and should not be "
"used in portable programs."
msgstr ""
"Para transportabilidad, sólo se debería acceder a los datos auxiliares "
"usando las macros descritas aquí. B<CMSG_ALIGN>() es una extensión de Linux "
"y no debería usarse en programas transportables."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "In Linux, B<CMSG_LEN>, B<CMSG_DATA>, and B<CMSG_ALIGN> are constant "
#| "expressions (assuming their argument is constant) - this could be used to "
#| "declare the size of global variables. This may be not portable, however."
msgid ""
"In Linux, B<CMSG_LEN>(), B<CMSG_DATA>(), and B<CMSG_ALIGN>()  are constant "
"expressions (assuming their argument is constant), meaning that these values "
"can be used to declare the size of global variables.  This may not be "
"portable, however."
msgstr ""
"En Linux, B<CMSG_LEN>, B<CMSG_DATA> y B<CMSG_ALIGN> son expresiones "
"constantes (suponiendo que su argumento sea contante). Esto se podría usar "
"para declarar el tamaño de variables globales pero, sin embargo, podría no "
"ser transportable."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EJEMPLOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This code looks for the B<IP_TTL> option in a received ancillary buffer:"
msgstr "Este código busca la opción B<IP_TTL> en un buffer auxiliar recibido:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"struct msghdr msgh;\n"
"struct cmsghdr *cmsg;\n"
"int received_ttl;\n"
msgstr ""
"struct msghdr msgh;\n"
"struct cmsghdr *cmsg;\n"
"int received_ttl;\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "/* Receive auxiliary data in msgh */\n"
msgstr "/* Recibir los datos auxiliares en msgh */\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"for (cmsg = CMSG_FIRSTHDR(&msgh); cmsg != NULL;\n"
"        cmsg = CMSG_NXTHDR(&msgh, cmsg)) {\n"
"    if (cmsg-E<gt>cmsg_level == IPPROTO_IP\n"
"            && cmsg-E<gt>cmsg_type == IP_TTL) {\n"
"        memcpy(&receive_ttl, CMSG_DATA(cmsg), sizeof(received_ttl));\n"
"        break;\n"
"    }\n"
"}\n"
msgstr ""
"for (cmsg = CMSG_FIRSTHDR(&msgh); cmsg != NULL;\n"
"        cmsg = CMSG_NXTHDR(&msgh, cmsg)) {\n"
"    if (cmsg-E<gt>cmsg_level == IPPROTO_IP\n"
"            && cmsg-E<gt>cmsg_type == IP_TTL) {\n"
"        memcpy(&receive_ttl, CMSG_DATA(cmsg), sizeof(received_ttl));\n"
"        break;\n"
"    }\n"
"}\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"if (cmsg == NULL) {\n"
"    /* Error: IP_TTL not enabled or small buffer or I/O error */\n"
"}\n"
msgstr ""
"if (cmsg == NULL) {\n"
"    /* Error: Error: IP_TTL no habilitada o buffer pequeño o error de E/S. */\n"
"}\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The code below passes an array of file descriptors over a Unix socket "
#| "using B<SCM_RIGHTS>:"
msgid ""
"The code below passes an array of file descriptors over a UNIX domain socket "
"using B<SCM_RIGHTS>:"
msgstr ""
"El siguiente código pasa un vector de descriptores de ficheros mediante un "
"conector Unix usando B<SCM_RIGHTS>:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"struct msghdr msg = { 0 };\n"
"struct cmsghdr *cmsg;\n"
"int myfds[NUM_FD];  /* Contains the file descriptors to pass */\n"
"char iobuf[1];\n"
"struct iovec io = {\n"
"    .iov_base = iobuf,\n"
"    .iov_len = sizeof(iobuf)\n"
"};\n"
"union {         /* Ancillary data buffer, wrapped in a union\n"
"                   in order to ensure it is suitably aligned */\n"
"    char buf[CMSG_SPACE(sizeof(myfds))];\n"
"    struct cmsghdr align;\n"
"} u;\n"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid ""
"msg.msg_iov = &io;\n"
"msg.msg_iovlen = 1;\n"
"msg.msg_control = u.buf;\n"
"msg.msg_controllen = sizeof(u.buf);\n"
"cmsg = CMSG_FIRSTHDR(&msg);\n"
"cmsg-E<gt>cmsg_level = SOL_SOCKET;\n"
"cmsg-E<gt>cmsg_type = SCM_RIGHTS;\n"
"cmsg-E<gt>cmsg_len = CMSG_LEN(sizeof(myfds));\n"
"memcpy(CMSG_DATA(cmsg), myfds, sizeof(myfds));\n"
msgstr ""
"msg.msg_iov = &io;\n"
"msg.msg_iovlen = 1;\n"
"msg.msg_control = u.buf;\n"
"msg.msg_controllen = sizeof(u.buf);\n"
"cmsg = CMSG_FIRSTHDR(&msg);\n"
"cmsg-E<gt>cmsg_level = SOL_SOCKET;\n"
"cmsg-E<gt>cmsg_type = SCM_RIGHTS;\n"
"cmsg-E<gt>cmsg_len = CMSG_LEN(sizeof(myfds));\n"
"memcpy(CMSG_DATA(cmsg), myfds, sizeof(myfds));\n"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "The code below passes an array of file descriptors over a Unix socket "
#| "using B<SCM_RIGHTS>:"
msgid ""
"For a complete code example that shows passing of file descriptors over a "
"UNIX domain socket, see B<seccomp_unotify>(2)."
msgstr ""
"El siguiente código pasa un vector de descriptores de ficheros mediante un "
"conector Unix usando B<SCM_RIGHTS>:"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<recvmsg>(2), B<sendmsg>(2)"
msgstr "B<recvmsg>(2), B<sendmsg>(2)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "RFC\\ 2292"
msgstr "RFC\\ 2292"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-11-01"
msgstr "1 Noviembre 2020"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual del Programador de Linux"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"B<#include E<lt>sys/socket.hE<gt>>\n"
"B<struct cmsghdr *CMSG_FIRSTHDR(struct msghdr *>I<msgh>B<);>\n"
"B<struct cmsghdr *CMSG_NXTHDR(struct msghdr *>I<msgh>B<,>\n"
"B<                            struct cmsghdr *>cmsgB<);>\n"
"B<size_t CMSG_ALIGN(size_t >I<length>B<);>\n"
"B<size_t CMSG_SPACE(size_t >I<length>B<);>\n"
"B<size_t CMSG_LEN(size_t >I<length>B<);>\n"
"B<unsigned char *CMSG_DATA(struct cmsghdr *>I<cmsg>B<);>\n"
msgstr ""
"B<#include E<lt>sys/socket.hE<gt>>\n"
"B<struct cmsghdr *CMSG_FIRSTHDR(struct msghdr *>I<msgh>B<);>\n"
"B<struct cmsghdr *CMSG_NXTHDR(struct msghdr *>I<msgh>B<,>\n"
"B<                            struct cmsghdr *>cmsgB<);>\n"
"B<size_t CMSG_ALIGN(size_t >I<length>B<);>\n"
"B<size_t CMSG_SPACE(size_t >I<length>B<);>\n"
"B<size_t CMSG_LEN(size_t >I<length>B<);>\n"
"B<unsigned char *CMSG_DATA(struct cmsghdr *>I<cmsg>B<);>\n"

#. type: SH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÓN"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.10 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 Marzo 2021"

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 5.13 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 Septiembre 2017"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<#include E<lt>sys/socket.hE<gt>>"
msgstr "B<#include E<lt>sys/socket.hE<gt>>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<struct cmsghdr *CMSG_FIRSTHDR(struct msghdr *>I<msgh>B<);>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<struct cmsghdr *CMSG_NXTHDR(struct msghdr *>I<msgh>B<, struct cmsghdr "
"*>I<cmsg>B<);>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<size_t CMSG_ALIGN(size_t >I<length>B<);>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<size_t CMSG_SPACE(size_t >I<length>B<);>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<size_t CMSG_LEN(size_t >I<length>B<);>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<unsigned char *CMSG_DATA(struct cmsghdr *>I<cmsg>B<);>"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "These macros are used to create and access control messages (also called "
#| "ancillary data) that are not a part of the socket payload.  This control "
#| "information may include the interface the packet was received on, various "
#| "rarely used header fields, an extended error description, a set of file "
#| "descriptors, or UNIX credentials.  For instance, control messages can be "
#| "used to send additional header fields such as IP options.  Ancillary data "
#| "is sent by calling B<sendmsg>(2)  and received by calling B<recvmsg>(2).  "
#| "See their manual pages for more information."
msgid ""
"These macros are used to create and access control messages (also called "
"ancillary data) that are not a part of the socket payload.  This control "
"information may include the interface the packet was received on, various "
"rarely used header fields, an extended error description, a set of file "
"descriptors or UNIX credentials.  For instance, control messages can be used "
"to send additional header fields such as IP options.  Ancillary data is sent "
"by calling B<sendmsg>(2)  and received by calling B<recvmsg>(2).  See their "
"manual pages for more information."
msgstr ""
"Estas macros se usan para crear y acceder a mensajes de control (también "
"llamados datos auxiliares) que no son parte del contenido útil de un "
"conector. Esta información de control puede incluir la interfaz en la que se "
"ha recibido el paquete, diferentes campos de cabecera usados raramente, una "
"descripción de error ampliada, un conjunto de descriptores de fichero o "
"credenciales de UNIX. Por ejemplo, los mensajes de control se pueden usar "
"para enviar campos de cabecera adicionales tales como opciones IP. Los datos "
"auxiliares se envían llamando a B<sendmsg>(2) y se reciben llamando a "
"B<recvmsg>(2). Vea sus páginas de manual para más información."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "B<CMSG_FIRSTHDR> returns a pointer to the first B<cmsghdr> in the "
#| "ancillary data buffer associated with the passed B<msghdr>."
msgid ""
"B<CMSG_FIRSTHDR>()  returns a pointer to the first I<cmsghdr> in the "
"ancillary data buffer associated with the passed I<msghdr>."
msgstr ""
"B<CMSG_FIRSTHDR> devuelve un puntero a la primera B<cmsghdr> en el buffer de "
"datos auxiliares asociado con la B<msghdr> pasada."

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<CMSG_DATA>()  returns a pointer to the data portion of a I<cmsghdr>."
msgstr ""
"B<CMSG_DATA>() devuelve un puntero a la porción de datos de una I<cmsghdr>."

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"When the control message buffer is too short to store all messages, the "
"B<MSG_CTRUNC> flag is set in the I<msg_flags> member of the I<msghdr>."
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This ancillary data model conforms to the POSIX.1g draft, 4.4BSD-Lite, the "
"IPv6 advanced API described in RFC\\ 2292 and SUSv2.  B<CMSG_ALIGN>()  is a "
"Linux extension."
msgstr ""
"El modelo de datos auxiliares sigue el borrador POSIX.1003.1g, 4.4BSD-Lite, "
"la API avanzada de IPv6 descrita en RFC\\ 2292 y SUSv2. B<CMSG_ALIGN>() es "
"una extensión de Linux."

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "In Linux, B<CMSG_LEN>, B<CMSG_DATA>, and B<CMSG_ALIGN> are constant "
#| "expressions (assuming their argument is constant) - this could be used to "
#| "declare the size of global variables. This may be not portable, however."
msgid ""
"In Linux, B<CMSG_LEN>(), B<CMSG_DATA>(), and B<CMSG_ALIGN>()  are constant "
"expressions (assuming their argument is constant); this could be used to "
"declare the size of global variables.  This may not be portable, however."
msgstr ""
"En Linux, B<CMSG_LEN>, B<CMSG_DATA> y B<CMSG_ALIGN> son expresiones "
"constantes (suponiendo que su argumento sea contante). Esto se podría usar "
"para declarar el tamaño de variables globales pero, sin embargo, podría no "
"ser transportable."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "EJEMPLO"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"struct msghdr msgh;\n"
"struct cmsghdr *cmsg;\n"
"int *ttlptr;\n"
"int received_ttl;\n"
msgstr ""
"struct msghdr msgh;\n"
"struct cmsghdr *cmsg;\n"
"int *ttlptr;\n"
"int received_ttl;\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"for (cmsg = CMSG_FIRSTHDR(&msgh); cmsg != NULL;\n"
"        cmsg = CMSG_NXTHDR(&msgh, cmsg)) {\n"
"    if (cmsg-E<gt>cmsg_level == IPPROTO_IP\n"
"            && cmsg-E<gt>cmsg_type == IP_TTL) {\n"
"        ttlptr = (int *) CMSG_DATA(cmsg);\n"
"        received_ttl = *ttlptr;\n"
"        break;\n"
"    }\n"
"}\n"
msgstr ""
"for (cmsg = CMSG_FIRSTHDR(&msgh); cmsg != NULL;\n"
"        cmsg = CMSG_NXTHDR(&msgh, cmsg)) {\n"
"    if (cmsg-E<gt>cmsg_level == IPPROTO_IP\n"
"            && cmsg-E<gt>cmsg_type == IP_TTL) {\n"
"        ttlptr = (int *) CMSG_DATA(cmsg);\n"
"        received_ttl = *ttlptr;\n"
"        break;\n"
"    }\n"
"}\n"

#. type: Plain text
#: opensuse-leap-15-5
#, no-wrap
msgid ""
"struct msghdr msg = { 0 };\n"
"struct cmsghdr *cmsg;\n"
"int myfds[NUM_FD];  /* Contains the file descriptors to pass */\n"
"int *fdptr;\n"
"char iobuf[1];\n"
"struct iovec io = {\n"
"    .iov_base = iobuf,\n"
"    .iov_len = sizeof(iobuf)\n"
"};\n"
"union {         /* Ancillary data buffer, wrapped in a union\n"
"                   in order to ensure it is suitably aligned */\n"
"    char buf[CMSG_SPACE(sizeof(myfds))];\n"
"    struct cmsghdr align;\n"
"} u;\n"
msgstr ""

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy, no-wrap
#| msgid ""
#| "msg.msg_iov = &io;\n"
#| "msg.msg_iovlen = 1;\n"
#| "msg.msg_control = u.buf;\n"
#| "msg.msg_controllen = sizeof(u.buf);\n"
#| "cmsg = CMSG_FIRSTHDR(&msg);\n"
#| "cmsg-E<gt>cmsg_level = SOL_SOCKET;\n"
#| "cmsg-E<gt>cmsg_type = SCM_RIGHTS;\n"
#| "cmsg-E<gt>cmsg_len = CMSG_LEN(sizeof(int) * NUM_FD);\n"
#| "memcpy(CMSG_DATA(cmsg), myfds, NUM_FD * sizeof(int));\n"
msgid ""
"msg.msg_iov = &io;\n"
"msg.msg_iovlen = 1;\n"
"msg.msg_control = u.buf;\n"
"msg.msg_controllen = sizeof(u.buf);\n"
"cmsg = CMSG_FIRSTHDR(&msg);\n"
"cmsg-E<gt>cmsg_level = SOL_SOCKET;\n"
"cmsg-E<gt>cmsg_type = SCM_RIGHTS;\n"
"cmsg-E<gt>cmsg_len = CMSG_LEN(sizeof(int) * NUM_FD);\n"
"fdptr = (int *) CMSG_DATA(cmsg);    /* Initialize the payload */\n"
"memcpy(fdptr, myfds, NUM_FD * sizeof(int));\n"
msgstr ""
"msg.msg_iov = &io;\n"
"msg.msg_iovlen = 1;\n"
"msg.msg_control = u.buf;\n"
"msg.msg_controllen = sizeof(u.buf);\n"
"cmsg = CMSG_FIRSTHDR(&msg);\n"
"cmsg-E<gt>cmsg_level = SOL_SOCKET;\n"
"cmsg-E<gt>cmsg_type = SCM_RIGHTS;\n"
"cmsg-E<gt>cmsg_len = CMSG_LEN(sizeof(int) * NUM_FD);\n"
"memcpy(CMSG_DATA(cmsg), myfds, NUM_FD * sizeof(int));\n"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página es parte de la versión 4.16 del proyecto Linux I<man-pages>. "
"Puede encontrar una descripción del proyecto, información sobre cómo "
"informar errores y la última versión de esta página en \\%https://www.kernel."
"org/doc/man-pages/."
