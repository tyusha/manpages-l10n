# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Gerardo Aburruzaga García <gerardo.aburruzaga@uca.es>, 1998.
# Juan Piernas <piernas@ditec.um.es>, 1999-2000.
# Marcos Fouces <marcos@debian.org>, 2020-2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-03 15:28+0200\n"
"PO-Revision-Date: 2022-05-02 18:52+0200\n"
"Last-Translator: Marcos Fouces <marcos@debian.org>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "CHMOD"
msgstr "CHMOD"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "April 2022"
msgstr "Abril de 2022"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Órdenes de usuario"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "chmod - change file mode bits"
msgstr "chmod - modifica los permisos de archivos"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<chmod> [I<\\,OPTION\\/>]... I<\\,MODE\\/>[I<\\,,MODE\\/>]... I<\\,FILE\\/"
">..."
msgstr ""
"B<chmod> [I<\\,OPCIÓN\\/>]... I<\\,MODO\\/>[I<\\,,MOD=\\/>]... I<\\,"
"FICHERO\\/>..."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<chmod> [I<\\,OPTION\\/>]... I<\\,OCTAL-MODE FILE\\/>..."
msgstr "B<chmod> [I<\\,OPCIÓN\\/>]... I<\\,MODO-OCTAL FICHERO\\/>..."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<chmod> [I<\\,OPTION\\/>]... I<\\,--reference=RFILE FILE\\/>..."
msgstr ""
"B<chmod> [I<\\,OPCIÓN\\/>]... I<\\,--reference=FICHERO-R FICHERO\\/>..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This manual page documents the GNU version of B<chmod>.  B<chmod> changes "
"the file mode bits of each given file according to I<mode>, which can be "
"either a symbolic representation of changes to make, or an octal number "
"representing the bit pattern for the new mode bits."
msgstr ""
"Esta página de manual describe la versión GNU de B<chmod>. B<chmod> modifica "
"los permisos de acceso de cada archivo conforme a I<modo>, que puede ser una "
"representación simbólica de dichas modificaciones o un número octal que "
"representa el patrón de bits para el nuevo modo."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The format of a symbolic mode is [B<ugoa>.\\|.\\|.][[B<-+=>][I<perms>.\\|."
"\\|.].\\|.\\|.], where I<perms> is either zero or more letters from the set "
"B<rwxXst>, or a single letter from the set B<ugo>.  Multiple symbolic modes "
"can be given, separated by commas."
msgstr ""
"El formato del modo simbólico es  [B<ugoa>.\\|.\\|.][[B<-+=>][I<perms>.\\|."
"\\|.].\\|.\\|.], donde I<perms> consta de cero o más letras entre B<rwxXst>, "
"o una sola letra entre B<ugo>. Se pueden dar varios modos simbólicos, "
"separados por comas."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A combination of the letters B<ugoa> controls which users' access to the "
"file will be changed: the user who owns it (B<u>), other users in the file's "
"group (B<g>), other users not in the file's group (B<o>), or all users "
"(B<a>).  If none of these are given, the effect is as if (B<a>) were given, "
"but bits that are set in the umask are not affected."
msgstr ""
"Una combinación de las letras B<ugoa> controla el acceso del que dispondrán "
"los usuarios al archivo sobre el que se aplique: el usuario que lo posee "
"(B<u>), otros usuarios del mismo grupo (B<g>), otros usuarios que no están "
"en el grupo del archivo (B<o>) o todos los usuarios (B<a>). Si no se "
"especifica ninguno, se sobreentiende (B<a>), pero los permisos establecidos "
"por la máscara (umask) no se ven afectados."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The operator B<+> causes the selected file mode bits to be added to the "
"existing file mode bits of each file; B<-> causes them to be removed; and "
"B<=> causes them to be added and causes unmentioned bits to be removed "
"except that a directory's unmentioned set user and group ID bits are not "
"affected."
msgstr ""
"El operador B<+> se emplea para añadir un determinado modo al archivo, B<-> "
"se usa para quitarlo y B<=> se usa para añadir los modos que se especifiquen "
"eliminando los demás salvo que se trate de SETUID o SETGID."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The letters B<rwxXst> select file mode bits for the affected users: read "
"(B<r>), write (B<w>), execute (or search for directories)  (B<x>), execute/"
"search only if the file is a directory or already has execute permission for "
"some user (B<X>), set user or group ID on execution (B<s>), restricted "
"deletion flag or sticky bit (B<t>).  Instead of one or more of these "
"letters, you can specify exactly one of the letters B<ugo>: the permissions "
"granted to the user who owns the file (B<u>), the permissions granted to "
"other users who are members of the file's group (B<g>), and the permissions "
"granted to users that are in neither of the two preceding categories (B<o>)."
msgstr ""
"Las letras B<rwxXstZ> seleccionarán los permisos del usuario afectado: "
"lectura (B<r>), escritura (B<w>), ejecución o permiso de búsqueda si se "
"trata de un directorio (B<x>), ejecución/búsqueda solo si se trata de un "
"directorio o ya tiene permiso de ejecución para algún usuario (B<X>), SETUID "
"o SETGID en la ejecución (B<s>), borrado restringido o \"sticky "
"bit\" (B<t>). En lugar de escribir una o más letras, puede indicar una de "
"las letras B<ugo>: las permisos del propietario del archivo  (B<u>), los "
"permisos de los otros usuarios que pertenecen al mismo grupo que el archivo  "
"(B<g>) y los permisos otorgados al resto de usuarios (B<o>)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"A numeric mode is from one to four octal digits (0-7), derived by adding up "
"the bits with values 4, 2, and 1.  Omitted digits are assumed to be leading "
"zeros.  The first digit selects the set user ID (4) and set group ID (2) and "
"restricted deletion or sticky (1) attributes.  The second digit selects "
"permissions for the user who owns the file: read (4), write (2), and execute "
"(1); the third selects permissions for other users in the file's group, with "
"the same values; and the fourth for other users not in the file's group, "
"with the same values."
msgstr ""
"Un modo numérico consta de entre uno y cuatro dígitos octales (0-7), que se "
"obtienen sumando los bits con los valores 4, 2 y 1.  Los dígitos que faltan "
"se consideran ceros y se colocan al incio. El primer dígito selecciona el ID "
"de usuario configurado (4), el ID de grupo configurado (2), por último, los "
"atributos de eliminación restringida o sticky bit (1). El segundo dígito "
"selecciona los permisos para el propietario del archivo: leer (4), escribir "
"(2) y ejecutar (1); el tercero selecciona permisos para otros usuarios en el "
"grupo del archivo y el cuarto para otros usuarios que no están en el grupo "
"del archivo, ambos con los mismos rangos de valores."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<chmod> never changes the permissions of symbolic links; the B<chmod> "
"system call cannot change their permissions.  This is not a problem since "
"the permissions of symbolic links are never used.  However, for each "
"symbolic link listed on the command line, B<chmod> changes the permissions "
"of the pointed-to file.  In contrast, B<chmod> ignores symbolic links "
"encountered during recursive directory traversals."
msgstr ""
"B<chmod> nunca cambia los permisos de enlaces simbólicos; la llamada del "
"sistema B<chmod>(2) no puede cambiar sus permisos. Esto no representa ningún "
"problema puesto que los permisos de los enlaces simbólicos nunca se usan. "
"Sin embargo, para cada enlace simbólico puesto en la línea de órdenes, "
"B<chmod> cambia los permisos del fichero al cual apunta. En cambio, B<chmod> "
"hará caso omiso de los enlaces simbólicos que encuentre durante el recorrido "
"recursivo de directorios."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SETUID AND SETGID BITS"
msgstr "BITS SETUID Y SETGID"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<chmod> clears the set-group-ID bit of a regular file if the file's group "
"ID does not match the user's effective group ID or one of the user's "
"supplementary group IDs, unless the user has appropriate privileges.  "
"Additional restrictions may cause the set-user-ID and set-group-ID bits of "
"I<MODE> or I<RFILE> to be ignored.  This behavior depends on the policy and "
"functionality of the underlying B<chmod> system call.  When in doubt, check "
"the underlying system behavior."
msgstr ""
"B<chmod> borra el bit SETGID de un archivo normal si el ID de grupo del "
"archivo no coincide con el ID de grupo efectivo del usuario o con uno de los "
"ID de grupo al que también pertenezca el usuario . salvo que el usuario "
"tenga los privilegios adecuados. Las restricciones adicionales pueden hacer "
"que se ignoren los bits SETUID y SETGID de I<MODE> o I<RFILE>. Esto depende "
"del comportamiento y de la funcionalidad de la llamada del sistema B<chmod> "
"subyacente. En caso de duda, compruebe el comportamiento del sistema "
"subyacente."

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"For directories B<chmod> preserves set-user-ID and set-group-ID bits unless "
"you explicitly specify otherwise.  You can set or clear the bits with "
"symbolic modes like B<u+s> and B<g-s>.  To clear these bits for directories "
"with a numeric mode requires an additional leading zero like B<00755>, "
"leading minus like B<-6000>, or leading equals like B<=755>."
msgstr ""
"En el caso de los directorios B<chmod> conserva los bits SETUID y SETGID "
"salvo que se especifique expresamente lo contrario. Puede establecer o "
"borrar los bits usando modos simbólicos por ejemplo B<u + s> y B<g-s>. Para "
"borrar estos bits de un directorio usando el modo numérico se requiere un "
"cero inicial adicional como B<00755>, o un signo menos al inicio: B<-600>, o "
"un signo igual también al inicio: B<=755>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "RESTRICTED DELETION FLAG OR STICKY BIT"
msgstr "MARCA DE BORRADO RESTRINGIDO O STICKY BIT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The restricted deletion flag or sticky bit is a single bit, whose "
"interpretation depends on the file type.  For directories, it prevents "
"unprivileged users from removing or renaming a file in the directory unless "
"they own the file or the directory; this is called the I<restricted deletion "
"flag> for the directory, and is commonly found on world-writable directories "
"like B</tmp>.  For regular files on some older systems, the bit saves the "
"program's text image on the swap device so it will load more quickly when "
"run; this is called the I<sticky bit>."
msgstr ""
"La marca de eliminación restringida o sticky bit es un solo bit, cuya "
"interpretación depende del tipo de archivo. En el caso de los directorios, "
"su uso evitará que los usuarios sin privilegios eliminen o cambien el nombre "
"de un archivo en el directorio a menos que sean propietarios del archivo o "
"del directorio; esto se denomina I<marca de eliminación restringida> para el "
"directorio, y se encuentra comúnmente en directorios donde todos los "
"usuarios tienen derecho de escritura como B</tmp>. Para archivos normales en "
"algunos sistemas más antiguos, la presencia de este bit hará que se guarde "
"la imagen del programa en el dispositivo de intercambio para que tarde menos "
"en cargarse cuando se ejecute; esto se llama I<sticky bit> (bit pegajoso)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Change the mode of each FILE to MODE.  With B<--reference>, change the mode "
"of each FILE to that of RFILE."
msgstr ""
"Cambia el modo de cada FICHERO a MODO. Con B<--reference>, cambia el modo de "
"cada FICHERO al de FICHERO-R."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--changes>"
msgstr "B<-c>, B<--changes>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "like verbose but report only when a change is made"
msgstr "como `verbose' pero informa solo si se hizo algún cambio"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--silent>, B<--quiet>"
msgstr "B<-f>, B<--silent>, B<--quiet>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "suppress most error messages"
msgstr "suprime la mayoría de los mensajes de error"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output a diagnostic for every file processed"
msgstr "muestra un mensaje por cada archivo procesado"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-preserve-root>"
msgstr "B<--no-preserve-root>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not treat '/' specially (the default)"
msgstr "no trata '/' de forma especial (predeterminado)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--preserve-root>"
msgstr "B<--preserve-root>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "fail to operate recursively on '/'"
msgstr "no opera recursivamente sobre '/'"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--reference>=I<\\,RFILE\\/>"
msgstr "B<--reference>=I<\\,R-ARCHIVO\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use RFILE's mode instead of MODE values"
msgstr "utiliza el modo de FICHERO-R en lugar del valor MODO"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--recursive>"
msgstr "B<-R>, B<--recursive>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "change files and directories recursively"
msgstr "cambia ficheros y directorios recursivamente"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "muestra la ayuda y finaliza"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "muestra la versión del programa y finaliza"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Each MODE is of the form '[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+'."
msgstr ""
"Cada MODO es de la forma '[ugoa]*([-+=]([rwxXst]*|[ugo]))+|[-+=][0-7]+'."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by David MacKenzie and Jim Meyering."
msgstr "Escrito por David MacKenzie y Jim Meyering."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "INFORMAR DE ERRORES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Ayuda en línea de GNU Coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Informe cualquier error de traducción a E<lt>https://translationproject.org/"
"team/es.htmlE<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Licencia GPLv3+: GNU GPL "
"versión 3 o posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Esto es software libre: usted es libre de cambiarlo y redistribuirlo.  NO "
"HAY GARANTÍA, en la medida permitida por la legislación."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<chmod>(2)"
msgstr "B<chmod>(2)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chmodE<gt>"
msgstr ""
"Ayuda en línea de GNU Coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) chmod invocation\\(aq"
msgstr ""
"también disponible localmente ejecutando: info \\(aq(coreutils) chmod "
"invocation\\(aq"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Septiembre de 2020"

#. type: TH
#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"For directories B<chmod> preserves set-user-ID and set-group-ID bits unless "
"you explicitly specify otherwise.  You can set or clear the bits with "
"symbolic modes like B<u+s> and B<g-s>.  To clear these bits for directories "
"with a numeric mode requires an additional leading zero, or leading = like "
"B<00755> , or B<=755>"
msgstr ""
"En el caso de los directorios B<chmod> conserva los bits SETUID y SETGID "
"salvo que se especifique expresamente lo contrario. Puede establecer o "
"borrar los bits usando modos simbólicos por ejemplo B<u + s> y B<g-s>. Para "
"borrar estos bits de un directorio usando el modo numérico se requiere un "
"cero inicial adicional, o un signo igual al inicio: B<00755> o B<=755>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Licencia GPLv3+: GNU GPL "
"versión 3 o posterior E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-5
msgid "chmod(2)"
msgstr "chmod(2)"

#. type: TH
#: debian-unstable fedora-rawhide
#, no-wrap
msgid "September 2022"
msgstr "Septiembre de 2022"

#. type: TH
#: fedora-37 opensuse-tumbleweed
#, no-wrap
msgid "August 2022"
msgstr "Agosto de 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "October 2021"
msgstr "Octubre de 2021"
