# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Enrique Ferrero Puchades <enferpuc@olemail.com>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-08-19 18:46+0200\n"
"PO-Revision-Date: 1999-05-28 19:53+0200\n"
"Last-Translator: Enrique Ferrero Puchades <enferpuc@olemail.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "EPSFFIT"
msgstr "EPSFFIT"

#. type: TH
#: archlinux
#, no-wrap
msgid "May 2022"
msgstr "Mayo de 2022"

#. type: TH
#: archlinux
#, fuzzy, no-wrap
#| msgid "epsffit 2.04"
msgid "epsffit 2.09"
msgstr "epsffit 2.04"

#. type: TH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Órdenes de usuario"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "epsffit - fit encapsulated PostScript file (EPSF) into constrained size"
msgid "epsffit - fit an Encapsulated PostScript file to a given bounding box"
msgstr ""
"epsffit - encaja ficheros PostScript encapsulados (EPSF) en un tamaño "
"determinado"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"B<epsffit> [I<\\,OPTION\\/>...] I<\\,LLX LLY URX URY \\/>[I<\\,INFILE \\/"
">[I<\\,OUTFILE\\/>]]"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "epsffit - fit encapsulated PostScript file (EPSF) into constrained size"
msgid "Fit an Encapsulated PostScript file to a given bounding box."
msgstr ""
"epsffit - encaja ficheros PostScript encapsulados (EPSF) en un tamaño "
"determinado"

#. type: TP
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--center>"
msgstr "B<-c>, B<--center>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "center the image in the given bounding box"
msgstr "centra la imagen en la caja de tamaño dado"

#. type: TP
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--rotate>"
msgstr "B<-r>, B<--rotate>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "rotate the image by 90 degrees counter-clockwise"
msgstr "gira la imagen 90 grados en el sentido de las agujas del reloj"

#. type: TP
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--aspect>"
msgstr "B<-a>, B<--aspect>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "adjust the aspect ratio to fit the bounding box"
msgstr "ajusta el tamaño de la presentación para que quepa en la caja"

#. type: TP
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--maximize>"
msgstr "B<-m>, B<--maximize>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "rotate the image to fill more of the page if possible"
msgstr ""

#. type: TP
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--showpage>"
msgstr "B<-s>, B<--showpage>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Add a I<showpage> at the end of the file to force the image to print."
msgid "append a I<\\,/showpage\\/> to the file to force printing"
msgstr ""
"Añade un I<showpage> al final del fichero para forzar la impresión de la "
"imagen."

#. type: TP
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "display this help and exit"
msgstr "muestra la ayuda y finaliza"

#. type: TP
#: archlinux
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "display version information and exit"
msgstr "mostrar información de versión y finalizar"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"(LLX, LLY) are the coordinates of the lower left corner of the box, and "
"(URX, URY) the upper right."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"If OUTFILE is not specified, writes to standard output.  If INFILE is not "
"specified, reads from standard input."
msgstr ""

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "See B<psutils>(1)  for the available units."
msgstr ""

#. type: SS
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "Exit status:"
msgstr "Estado de salida:"

#. type: TP
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "0"
msgstr "0"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "if OK,"
msgstr "si todo fue bien,"

#. type: TP
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "1"
msgstr "1"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"if arguments or options are incorrect, or there is some other problem "
"starting up,"
msgstr ""

#. type: TP
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "2"
msgstr "2"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"if there is some problem during processing, typically an error reading or "
"writing an input or output file."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "Written by Angus J. C. Duggan and Reuben Thomas."
msgstr "Escrito por Angus J. C. Duggan y Reuben Thomas."

#. type: SH
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux
msgid ""
"Copyright \\(co Reuben Thomas 2016-2022.  Released under the GPL version 3, "
"or (at your option) any later version."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "TRADEMARKS"
msgstr "MARCAS REGISTRADAS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<PostScript> is a trademark of Adobe Systems Incorporated."
msgstr "B<PostScript> es una marca registrada de Adobe Systems Incorporated."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "B<psutils>(1), B<paper>(1)"
msgstr "B<psutils>(1), B<paper>(1)"

#. type: TH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "PSUtils Release 1 Patchlevel 17"
msgstr "PSUtils Release 1 Patchlevel 17"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "epsffit - fit encapsulated PostScript file (EPSF) into constrained size"
msgstr ""
"epsffit - encaja ficheros PostScript encapsulados (EPSF) en un tamaño "
"determinado"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"B<epsffit> [ B<-c> ] [ B<-r> ] [ B<-a> ] [ B<-m> ] [ B<-s> ] I<llx lly urx "
"ury> [ B<infile> [ B<outfile> ] ]"
msgstr ""
"B<epsffit> [ B<-c> ] [ B<-r> ] [ B<-a> ] [ B<-m> ] [ B<-s> ] I<llx lly urx "
"ury> [ B<infile> [ B<outfile> ] ]"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"I<Epsffit> fits an EPSF file (encapsulated PostScript) to a given bounding "
"box.  The coordinates of the box are given by B<(llx,lly)> for the lower "
"left, and B<(urx,ury)> for the upper right, in PostScript units (points)."
msgstr ""
"I<Epsffit> encaja un fichero EPSF (PostScript encapsulado) en una caja de "
"tamaño determinado. Las coordenadas de la caja se especifican con B<(llx,"
"lly)> para la esquina inferior izquierda, y B<(urx,ury)> para la superior "
"derecha, en unidades PostScript (puntos)."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"If no input or output files are specified, I<epsffit> read from the standard "
"input and writes to the standard output."
msgstr ""
"Si no se especifican ficheros de entrada o salida, I<epsffit> lee de la "
"entrada estándar y escribe en la salida estándar."

#. type: SH
#: debian-bullseye debian-unstable
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-c>"
msgstr "B<-c>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Center the image in the given bounding box."
msgstr "Centra la imagen en la caja de tamaño dado."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-r>"
msgstr "B<-r>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Rotate the image by 90 degrees counter-clockwise."
msgstr "Gira la imagen 90 grados en el sentido de las agujas del reloj."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-a>"
msgstr "B<-a>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Adjust the aspect ratio to fit the bounding box. The default is to preserve "
"the aspect ratio."
msgstr ""
"Ajusta el tamaño de la presentación para que quepa en la caja. Por defecto "
"preserva el tamaño original."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-m>"
msgstr "B<-m>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Rotates the image to maximise the size if it would fit the specified "
"bounding box better."
msgstr ""
"Gira la imagen para maximizar el tamaño si con ello cabe mejor en la caja "
"especificada."

#. type: IP
#: debian-bullseye debian-unstable
#, no-wrap
msgid "B<-s>"
msgstr "B<-s>"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Add a I<showpage> at the end of the file to force the image to print."
msgstr ""
"Añade un I<showpage> al final del fichero para forzar la impresión de la "
"imagen."

#. type: Plain text
#: debian-bullseye debian-unstable
msgid "Copyright (C) Angus J. C. Duggan 1991-1995"
msgstr "Copyright (C) Angus J. C. Duggan 1991-1995"

#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"
msgstr ""
"psbook(1), psselect(1), pstops(1), epsffit(1), psnup(1), psresize(1), "
"psmerge(1), fixscribeps(1), getafm(1), fixdlsrps(1), fixfmps(1), "
"fixpsditps(1), fixpspps(1), fixtpps(1), fixwfwps(1), fixwpps(1), fixwwps(1), "
"extractres(1), includeres(1), showchar(1)"

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr "Octubre de 2021"

#. type: TH
#: fedora-37 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "epsffit 2.04"
msgid "epsffit 2.07"
msgstr "epsffit 2.04"

#. type: TP
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co Reuben Thomas 2016.  Released under the GPL version 3, or "
"(at your option) any later version."
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "December 2021"
msgstr "Diciembre de 2021"

#. type: TH
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "epsffit 2.04"
msgid "epsffit 2.08"
msgstr "epsffit 2.04"
