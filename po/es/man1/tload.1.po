# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Pedro Pablo Fábrega <pfabrega@arrakis.es>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-03 16:01+0200\n"
"PO-Revision-Date: 1999-02-21 19:53+0200\n"
"Last-Translator: Pedro Pablo Fábrega <pfabrega@arrakis.es>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "TLOAD"
msgstr "TLOAD"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "June 2011"
msgstr "Junio de 2011"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "User Commands"
msgstr "Órdenes de usuario"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: opensuse-leap-15-5
msgid "tload - graphic representation of system load average"
msgstr "tload - representación gráfica de la carga promedio del sistema"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<tload> [I<options>] [I<tty>]"
msgstr "B<tload> [I<opciones>] [I<tty>]"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: opensuse-leap-15-5
#, fuzzy
#| msgid ""
#| "B<tload> prints a graph of the current system load average to the "
#| "specified I<tty> (or the tty of the tload process if none is specified)."
msgid ""
"B<tload> prints a graph of the current system load average to the specified "
"I<tty> (or the tty of the tload process if none is specified)."
msgstr ""
"B<tload> imprime un gráfico del promedio de carga del sistema actual en la "
"I<tty> especificada (o la tty del proceso tload si no se especifica ninguna)."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-s>, B<--scale> I<number>"
msgstr "B<-s>, B<--scale> I<número>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The scale option allows a vertical scale to be specified for the display (in "
"characters between graph ticks); thus, a smaller value represents a larger "
"scale, and vice versa."
msgstr ""
"La opción I<scale> permite especificar una escala vertical para la salida "
"(en caracteres entre marcas gráficas); así, un valor más pequeño representa "
"una escala más grande, y viceversa."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-d>, B<--delay> I<seconds>"
msgstr "B<-d>, B<--delay> I<segundos>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "The delay sets the delay between graph updates in I<seconds>."
msgstr ""
"El retardo fija el retardo entre actualizaciones gráficas en I<segundos>."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display this help text."
msgstr "Muestra la ayuda."

#. type: TP
#: opensuse-leap-15-5
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Display version information and exit."
msgstr "Muestra información acerca de la versión y finaliza."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: Plain text
#: opensuse-leap-15-5
msgid "I</proc/loadavg> load average information"
msgstr "I</proc/loadavg> información de la carga promedio"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: opensuse-leap-15-5
msgid "B<ps>(1), B<top>(1), B<uptime>(1), B<w>(1)"
msgstr "B<ps>(1), B<top>(1), B<uptime>(1), B<w>(1)"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"The B<-d>I< delay> option sets the time argument for an B<alarm>(2); if -d 0 "
"is specified, the alarm is set to 0, which will never send the B<SIGALRM> "
"and update the display."
msgstr ""
"La opción B<-d>I< retardo> fija el argumento tiempo para B<alarm>(2); si se "
"especifica -d 0, la alarma se pone a 0, con lo cual nunca se enviará la "
"B<SIGALRM> ni se actualizará el gráfico."

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORES"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Branko Lankester, E<.UR david@\\:ods.\\:com> David Engel E<.UE , and> E<.UR "
"johnsonm@\\:redhat.\\:com> Michael K. Johnson E<.UE .>"
msgstr ""
"Branko Lankester, E<.UR david@\\:ods.\\:com> David Engel E<.UE , y> E<.UR "
"johnsonm@\\:redhat.\\:com> Michael K. Johnson E<.UE .>"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "REPORTING BUGS"
msgstr "INFORMAR DE ERRORES"

#. type: Plain text
#: opensuse-leap-15-5
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Por favor, notifique cualquier error a E<.UR procps@freelists.org> E<.UE>"
