# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2013, 2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2020
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2022-10-17 20:32+0200\n"
"PO-Revision-Date: 2020-04-10 07:49+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "LOCALE"
msgstr "LOCALE"

#. type: TH
#: archlinux
#, no-wrap
msgid "2022-09-09"
msgstr "9 septembre 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr "Pages du manuel de Linux (inédites)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "locale - get locale-specific information"
msgstr "locale - Obtenir des informations sur les paramètres régionaux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<locale> [I<option>]\n"
"B<locale> [I<option>] B<-a>\n"
"B<locale> [I<option>] B<-m>\n"
"B<locale> [I<option>] I<name>...\n"
msgstr ""
"B<locale> [I<option>]\n"
"B<locale> [I<option>] B<-a>\n"
"B<locale> [I<option>] B<-m>\n"
"B<locale> [I<option>] I<nom>...\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<locale> command displays information about the current locale, or all "
"locales, on standard output."
msgstr ""
"La commande B<locale> affiche des informations sur la localisation (NdT : la "
"localisation est aussi appelée paramètres régionaux, ou « locale » en "
"anglais) en cours ou pour toutes les localisations, sur la sortie standard."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"When invoked without arguments, B<locale> displays the current locale "
"settings for each locale category (see B<locale>(5)), based on the settings "
"of the environment variables that control the locale (see B<locale>(7)).  "
"Values for variables set in the environment are printed without double "
"quotes, implied values are printed with double quotes."
msgstr ""
"Appelée sans paramètre, B<locale> affiche les définitions en cours de chaque "
"catégorie des paramètres régionaux (voir B<locale>(5)), en fonction des "
"variables d'environnement contrôlant la localisation (voir B<locale>(7)). "
"Les valeurs des variables définies dans l'environnement sont affichées sans "
"double-guillemets, les valeurs implicites sont affichées avec."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If either the B<-a> or the B<-m> option (or one of their long-format "
"equivalents) is specified, the behavior is as follows:"
msgstr ""
"Si l'option B<-a> ou B<-m> (ou leur équivalent sous forme longue) est "
"indiquée, le comportement suivi est le suivant :"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--all-locales>"
msgstr "B<-a>, B<--all-locales>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Display a list of all available locales.  The B<-v> option causes the "
"B<LC_IDENTIFICATION> metadata about each locale to be included in the output."
msgstr ""
"Afficher la liste de toutes les localisations disponibles. L'option B<-v> "
"permet d'afficher les métadonnées B<LC_IDENTIFICATION> de chaque locale."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-m>, B<--charmaps>"
msgstr "B<-m>, B<--charmaps>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Display the available charmaps (character set description files).  To "
"display the current character set for the locale, use B<locale -c charmap>."
msgstr ""
"Afficher les tables de caractères disponibles (fichiers de description de "
"jeux de caractères). Pour afficher le jeu de caractères actuel pour vos "
"paramètres régionaux, utilisez I<locale -c charmap>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The B<locale> command can also be provided with one or more arguments, which "
"are the names of locale keywords (for example, I<date_fmt>, I<ctype-class-"
"names>, I<yesexpr>, or I<decimal_point>)  or locale categories (for example, "
"B<LC_CTYPE> or B<LC_TIME>).  For each argument, the following is displayed:"
msgstr ""
"La commande B<locale> peut être appelée avec un ou plusieurs paramètres qui "
"sont les mots-clés d'une localisation (par exemple I<date_fmt>, I<ctype-"
"class-names>, I<yesexpr> ou I<decimal_point>) ou les catégories d'une "
"localisation (par exemple B<LC_CTYPE> ou B<LC_TIME>). Pour chaque paramètre "
"est affiché :"

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "*"
msgstr "–"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "For a locale keyword, the value of that keyword to be displayed."
msgstr "Pour un mot-clé de localisation, la valeur de ce mot-clé est affichée."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For a locale category, the values of all keywords in that category are "
"displayed."
msgstr ""
"Pour une catégorie de localisation, les valeurs des mots-clés dans cette "
"catégorie sont affichées."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "When arguments are supplied, the following options are meaningful:"
msgstr ""
"Lorsque des paramètres sont fournis, les options suivantes sont applicables :"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--category-name>"
msgstr "B<-c>, B<--category-name>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For a category name argument, write the name of the locale category on a "
"separate line preceding the list of keyword values for that category."
msgstr ""
"Si le paramètre est un nom de catégorie, afficher le nom de la catégorie sur "
"une ligne distincte avant la liste des mots-clés de cette catégorie."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For a keyword name argument, write the name of the locale category for this "
"keyword on a separate line preceding the keyword value."
msgstr ""
"Si le paramètre est un mot-clé, afficher le nom de la catégorie de ce mot-"
"clé sur une ligne distincte avant la valeur du mot-clé."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This option improves readability when multiple name arguments are "
"specified.  It can be combined with the B<-k> option."
msgstr ""
"Cette option améliore la lisibilité lorsque plusieurs paramètres sont "
"indiqués. Elle peut être combinée avec l'option B<-k>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-k>, B<--keyword-name>"
msgstr "B<-k>, B<--keyword-name>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For each keyword whose value is being displayed, include also the name of "
"that keyword, so that the output has the format:"
msgstr ""
"Pour chaque mot-clé à afficher, inclure également le nom du mot-clé sous la "
"forme suivante :"

#. type: Plain text
#: archlinux
#, fuzzy, no-wrap
#| msgid "    I<keyword>=\"I<value>\"\n"
msgid "I<keyword>=\"I<value>\"\n"
msgstr "    I<mot-clé>=\"I<valeur>\"\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "The B<locale> command also knows about the following options:"
msgstr "La commande B<locale> reconnaît aussi les options suivantes :"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Display additional information for some command-line option and argument "
"combinations."
msgstr ""
"Afficher des informations supplémentaires sur les options et paramètres de "
"la ligne de commande."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-?>, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Display a summary of command-line options and arguments and exit."
msgstr ""
"Afficher un résumé des options et paramètres de la ligne de commande, puis "
"terminer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Display a short usage message and exit."
msgstr "Afficher un court message d'aide et terminer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Display the program version and exit."
msgstr "Afficher la version du programme et terminer."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/lib/locale/locale-archive>"
msgstr "I</usr/lib/locale/locale-archive>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Usual default locale archive location."
msgstr "Chemin habituel par défaut de l'archive des paramètres régionaux."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I</usr/share/i18n/locales>"
msgstr "I</usr/share/i18n/locales>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "Usual default path for locale definition files."
msgstr ""
"Chemin par défaut habituel des fichiers de définition de paramètres "
"régionaux."

#. type: SH
#: archlinux
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPLES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<locale>\n"
"LANG=en_US.UTF-8\n"
"LC_CTYPE=\"en_US.UTF-8\"\n"
"LC_NUMERIC=\"en_US.UTF-8\"\n"
"LC_TIME=\"en_US.UTF-8\"\n"
"LC_COLLATE=\"en_US.UTF-8\"\n"
"LC_MONETARY=\"en_US.UTF-8\"\n"
"LC_MESSAGES=\"en_US.UTF-8\"\n"
"LC_PAPER=\"en_US.UTF-8\"\n"
"LC_NAME=\"en_US.UTF-8\"\n"
"LC_ADDRESS=\"en_US.UTF-8\"\n"
"LC_TELEPHONE=\"en_US.UTF-8\"\n"
"LC_MEASUREMENT=\"en_US.UTF-8\"\n"
"LC_IDENTIFICATION=\"en_US.UTF-8\"\n"
"LC_ALL=\n"
msgstr ""
"$ B<locale>\n"
"LANG=fr_FR.UTF-8\n"
"LC_CTYPE=\"fr_FR.UTF-8\"\n"
"LC_NUMERIC=\"fr_FR.UTF-8\"\n"
"LC_TIME=\"fr_FR.UTF-8\"\n"
"LC_COLLATE=\"fr_FR.UTF-8\"\n"
"LC_MONETARY=\"fr_FR.UTF-8\"\n"
"LC_MESSAGES=\"fr_FR.UTF-8\"\n"
"LC_PAPER=\"fr_FR.UTF-8\"\n"
"LC_NAME=\"fr_FR.UTF-8\"\n"
"LC_ADDRESS=\"fr_FR.UTF-8\"\n"
"LC_TELEPHONE=\"fr_FR.UTF-8\"\n"
"LC_MEASUREMENT=\"fr_FR.UTF-8\"\n"
"LC_IDENTIFICATION=\"fr_FR.UTF-8\"\n"
"LC_ALL=\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<locale date_fmt>\n"
"%a %b %e %H:%M:%S %Z %Y\n"
msgstr ""
"$ B<locale date_fmt>\n"
"%A %-e %B %Y, %H:%M:%S (UTC%z)\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<locale -k date_fmt>\n"
"date_fmt=\"%a %b %e %H:%M:%S %Z %Y\"\n"
msgstr ""
"$ B<locale -k date_fmt>\n"
"date_fmt=\"%A %-e %B %Y, %H:%M:%S (UTC%z)\"\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<locale -ck date_fmt>\n"
"LC_TIME\n"
"date_fmt=\"%a %b %e %H:%M:%S %Z %Y\"\n"
msgstr ""
"$ B<locale -ck date_fmt>\n"
"LC_TIME\n"
"date_fmt=\"%A %-e %B %Y, %H:%M:%S (UTC%z)\"\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<locale LC_TELEPHONE>\n"
"+%c (%a) %l\n"
"(%a) %l\n"
"11\n"
"1\n"
"UTF-8\n"
msgstr ""
"$ B<locale LC_TELEPHONE>\n"
"+%c (%a) %l\n"
"(%a) %l\n"
"11\n"
"1\n"
"UTF-8\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<locale -k LC_TELEPHONE>\n"
"tel_int_fmt=\"+%c (%a) %l\"\n"
"tel_dom_fmt=\"(%a) %l\"\n"
"int_select=\"11\"\n"
"int_prefix=\"1\"\n"
"telephone-codeset=\"UTF-8\"\n"
msgstr ""
"$ B<locale -k LC_TELEPHONE>\n"
"tel_int_fmt=\"+%c (%a) %l\"\n"
"tel_dom_fmt=\"(%a) %l\"\n"
"int_select=\"11\"\n"
"int_prefix=\"1\"\n"
"telephone-codeset=\"UTF-8\"\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The following example compiles a custom locale from the I<./wrk> directory "
"with the B<localedef>(1)  utility under the I<$HOME/.locale> directory, then "
"tests the result with the B<date>(1)  command, and then sets the environment "
"variables B<LOCPATH> and B<LANG> in the shell profile file so that the "
"custom locale will be used in the subsequent user sessions:"
msgstr ""
"L'exemple ci-dessous compile avec l'outil B<localedef>(1) des paramètres "
"régionaux personnalisés à partir des fichiers se trouvant dans le répertoire "
"I<./wrk>. Les paramètres compilés sont stockés dans le répertoire I<$HOME/."
"locale>, puis le résultat est testé avec la commande B<date>(1), et enfin "
"les variables d'environnement B<LOCPATH> et B<LANG> sont positionnées dans "
"le fichier d'initialisation de l'interpréteur de commandes afin que ces "
"paramètres régionaux personnalisés soient disponibles dans les sessions "
"ultérieures :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid ""
"$ B<mkdir -p $HOME/.locale>\n"
"$ B<I18NPATH=./wrk/ localedef -f UTF-8 -i fi_SE $HOME/.locale/fi_SE.UTF-8>\n"
"$ B<LOCPATH=$HOME/.locale LC_ALL=fi_SE.UTF-8 date>\n"
"$ B<echo \"export LOCPATH=\\e$HOME/.locale\" E<gt>E<gt> $HOME/.bashrc>\n"
"$ B<echo \"export LANG=fi_SE.UTF-8\" E<gt>E<gt> $HOME/.bashrc>\n"
msgstr ""
"$ B<mkdir -p $HOME/.locale>\n"
"$ B<I18NPATH=./wrk/ localedef -f UTF-8 -i fi_SE $HOME/.locale/fi_SE.UTF-8>\n"
"$ B<LOCPATH=$HOME/.locale LC_ALL=fi_SE.UTF-8 date>\n"
"$ B<echo \"export LOCPATH=\\e$HOME/.locale\" E<gt>E<gt> $HOME/.bashrc>\n"
"$ B<echo \"export LANG=fi_SE.UTF-8\" E<gt>E<gt> $HOME/.bashrc>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<localedef>(1), B<charmap>(5), B<locale>(5), B<locale>(7)"
msgstr "B<localedef>(1), B<charmap>(5), B<locale>(5), B<locale>(7)"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 juin 2020"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "Linux User Manual"
msgstr "Manuel de l'utilisateur Linux"

#. type: Plain text
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "    I<keyword>=\"I<value>\"\n"
msgstr "    I<mot-clé>=\"I<valeur>\"\n"

#. type: SH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: SH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-unstable fedora-37 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 mars 2021"

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.13 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: SH
#: opensuse-leap-15-5
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPLE"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
