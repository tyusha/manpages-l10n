# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-03 15:53+0200\n"
"PO-Revision-Date: 2021-11-04 18:02+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SDIFF"
msgstr "SDIFF"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "August 2021"
msgstr "augusti 2021"

#. type: TH
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "diffutils 3.8"
msgstr "diffutils 3.8"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, fuzzy
#| msgid "Side-by-side merge of differences between FILE1 and FILE2."
msgid "sdiff - side-by-side merge of file differences"
msgstr "Sammanslagning sida-vid-sida av skillnader mellan FIL1 och FIL2."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<sdiff> [I<OPTION>]... I<FILE1 FILE2>"
msgstr "B<sdiff> [I<FLAGGA>]... I<FIL1 FIL2>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Side-by-side merge of differences between FILE1 and FILE2."
msgstr "Sammanslagning sida-vid-sida av skillnader mellan FIL1 och FIL2."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Obligatoriska argument till långa flaggor är obligatoriska även för de korta."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output>=I<FILE>"
msgstr "B<-o>, B<--output>=I<FIL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "operate interactively, sending output to FILE"
msgstr "Kör interaktivt, med utdata till FIL."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-case>"
msgstr "B<-i>, B<--ignore-case>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "consider upper- and lower-case to be the same"
msgstr "Betrakta versaler och gemena som lika."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-E>, B<--ignore-tab-expansion>"
msgstr "B<-E>, B<--ignore-tab-expansion>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore changes due to tab expansion"
msgstr "Ignorera ändringar på grund av tabulatorexpansion."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-Z>, B<--ignore-trailing-space>"
msgstr "B<-Z>, B<--ignore-trailing-space>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore white space at line end"
msgstr "Ignorera alla blanktecken i slutet av raden."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--ignore-space-change>"
msgstr "B<-b>, B<--ignore-space-change>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore changes in the amount of white space"
msgstr "Ignorera ändringar i antalet blanka."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-W>, B<--ignore-all-space>"
msgstr "B<-W>, B<--ignore-all-space>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore all white space"
msgstr "Ignorera alla blanktecken."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-B>, B<--ignore-blank-lines>"
msgstr "B<-B>, B<--ignore-blank-lines>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore changes whose lines are all blank"
msgstr "Ignorera ändringar i form av enbart blanka rader."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-I>, B<--ignore-matching-lines>=I<RE>"
msgstr "B<-I>, B<--ignore-matching-lines>=I<RU>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "ignore changes all whose lines match RE"
msgstr "Bortse från ändringar vars alla rader matchar RU."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--strip-trailing-cr>"
msgstr "B<--strip-trailing-cr>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "strip trailing carriage return on input"
msgstr "Tag bort avslutande vagnreturer i indata."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-a>, B<--text>"
msgstr "B<-a>, B<--text>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "treat all files as text"
msgstr "Betrakta alla filer som text."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--width>=I<NUM>"
msgstr "B<-w>, B<--width>=I<ANT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output at most NUM (default 130) print columns"
msgstr "Skriv ut högst ANT (130 som standard) kolumner."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--left-column>"
msgstr "B<-l>, B<--left-column>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output only the left column of common lines"
msgstr "Skriv endast ut gemensamma raders vänstra kolumn."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--suppress-common-lines>"
msgstr "B<-s>, B<--suppress-common-lines>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not output common lines"
msgstr "Skriv inte ut gemensamma rader."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--expand-tabs>"
msgstr "B<-t>, B<--expand-tabs>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "expand tabs to spaces in output"
msgstr "Expandera tabulatorer till blanksteg i utdata."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--tabsize>=I<NUM>"
msgstr "B<--tabsize>=I<N>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "tab stops at every NUM (default 8) print columns"
msgstr "Tabulatorstopp var N:e (8 som standard) kolumn."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-d>, B<--minimal>"
msgstr "B<-d>, B<--minimal>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "try hard to find a smaller set of changes"
msgstr "Arbeta hårt för att hitta en mindre mängd av ändringar."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>, B<--speed-large-files>"
msgstr "B<-H>, B<--speed-large-files>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "assume large files, many scattered small changes"
msgstr "Antag stora filer och små spridda ändringar."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--diff-program>=I<PROGRAM>"
msgstr "B<--diff-program>=I<PROGRAM>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use PROGRAM to compare files"
msgstr "Använd PROGRAM för att jämföra filer."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "Visa denna hjälp och avsluta."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr "B<-v>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "Visa versionsinformation och avsluta."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If a FILE is '-', read standard input.  Exit status is 0 if inputs are the "
"same, 1 if different, 2 if trouble."
msgstr ""
"Om en FIL är ”-”, läs standard in. Slutsstatus är 0 om indata är lika, 1 om "
"olika, 2 vid problem."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "UPPHOVSMAN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Thomas Lord."
msgstr "Skrivet av Thomas Lord."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Report bugs to: bug-diffutils@gnu.org"
msgstr ""
"Rapportera fel till: E<lt>bug-diffutils@gnu.orgE<gt>; rapportera synpunkter "
"på översättningen till: E<lt>tp-sv@listor.tp-sv.seE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"GNU diffutils home page: E<lt>https://www.gnu.org/software/diffutils/E<gt>"
msgstr ""
"GNU diffutils hemsida: E<lt>https://www.gnu.org/software/diffutils/E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "General help using GNU software: E<lt>https://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Allmän hjälp med att använda GNU-program: E<lt>https://www.gnu.org/gethelp/"
"E<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Detta är fri programvara: du får fritt ändra och vidaredistribuera den. Det "
"finns INGEN GARANTI, så långt lagen tillåter."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OCKSÅ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "cmp(1), diff(1), diff3(1)"
msgstr "B<cmp>(1), B<diff>(1), B<diff3>(1)"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The full documentation for B<sdiff> is maintained as a Texinfo manual.  If "
"the B<info> and B<sdiff> programs are properly installed at your site, the "
"command"
msgstr ""
"Den fullständiga dokumentationen för B<sdiff> underhålls som en Texinfo-"
"manual. Om programmen B<info> och B<sdiff> är ordentligt installerade på "
"ditt system, bör kommandot"

#. type: Plain text
#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<info sdiff>"
msgstr "B<info sdiff>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GNU"
msgstr "SDIFF"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "December 2018"
msgstr "december 2018"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "diffutils 3.7"
msgstr "diffutils 3.7"

#. type: Plain text
#: debian-bullseye
msgid "GNU sdiff - side-by-side merge of file differences"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2018 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: debian-bullseye
msgid ""
"The full documentation for B<GNU> is maintained as a Texinfo manual.  If the "
"B<info> and B<GNU> programs are properly installed at your site, the command"
msgstr ""
"Den fullständiga dokumentationen för B<sdiff> underhålls som en Texinfo-"
"manual. Om programmen B<info> och B<sdiff> är ordentligt installerade på "
"ditt system, bör kommandot"

#. type: Plain text
#: debian-bullseye
msgid "B<info GNU>"
msgstr "B<info sdiff>"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "May 2017"
msgstr "maj 2017"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "diffutils 3.6"
msgstr "diffutils 3.6"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"GNU diffutils home page: E<lt>http://www.gnu.org/software/diffutils/E<gt>"
msgstr ""
"GNU diffutils hemsida: E<lt>https://www.gnu.org/software/diffutils/E<gt>"

#. type: Plain text
#: opensuse-leap-15-5
msgid "General help using GNU software: E<lt>http://www.gnu.org/gethelp/E<gt>"
msgstr ""
"Allmän hjälp med att använda GNU-program: E<lt>http://www.gnu.org/gethelp/"
"E<gt>"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"Copyright \\(co 2017 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2017 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>http://gnu.org/licenses/gpl.htmlE<gt>."
