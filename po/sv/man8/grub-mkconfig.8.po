# Swedish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-17 20:27+0200\n"
"PO-Revision-Date: 2022-07-22 15:11+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-MKCONFIG"
msgstr "GRUB-MKCONFIG"

#. type: TH
#: archlinux
#, no-wrap
msgid "October 2022"
msgstr "oktober 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r334.g340377470-1"
msgstr "GRUB 2:2.06.r334.g340377470-1"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemadministrationsverktyg"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "grub-mkconfig - generate a GRUB configuration file"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-mkconfig> [I<\\,OPTION\\/>]"
msgstr "B<grub-mkconfig> [I<\\,FLAGGA\\/>]"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Generate a grub config file"
msgstr "Generera en konfigurationsfil för grub"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-o>, B<--output>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output>=I<\\,FIL\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "output generated config to FILE [default=stdout]"
msgstr "mata ut genererad konfiguration till FIL [standard=stdut]"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print this message and exit"
msgstr "skriv ut detta meddelande och avsluta"

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print the version information and exit"
msgstr "skriv ut versionsinformation och avsluta"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr ""
"Rapportera fel till E<lt>bug-grub@gnu.orgE<gt>. Skicka synpunkter på "
"översättningen till E<gt>tp-sv@listor.tp-sv.seE<lt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OCKSÅ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-install>(8)"
msgstr "B<grub-install>(8)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-mkconfig> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-mkconfig> programs are properly installed "
"at your site, the command"
msgstr ""
"Den fullständiga dokumentationen för B<grub-mkconfig> underhålls som en "
"Texinfo-manual. Om programmen B<info> och B<grub-mkconfig> är ordentligt "
"installerade på ditt system, bör kommandot"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-mkconfig>"
msgstr "B<info grub-mkconfig>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "August 2022"
msgstr "augusti 2022"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.06-3~deb11u1"
msgstr "GRUB 2.06-3~deb11u1"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.06-4"
msgstr "GRUB 2.06-4"
