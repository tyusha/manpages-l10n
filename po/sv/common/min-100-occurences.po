# Common msgids
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-10-17 21:05+0200\n"
"PO-Revision-Date: 2022-10-22 18:13+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swedish <>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: debian-bullseye debian-unstable archlinux fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed mageia-cauldron
#, no-wrap
msgid "*"
msgstr "*"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed mageia-cauldron archlinux opensuse-leap-15-4
#, no-wrap
msgid "AUTHOR"
msgstr "UPPHOVSMAN"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr ""

#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed debian-bullseye debian-unstable
#, no-wrap
msgid "AVAILABILITY"
msgstr "TILLGÄNGLIGHET"

#: debian-unstable mageia-cauldron archlinux fedora-37 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr "april 2022"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#: fedora-37 opensuse-tumbleweed debian-unstable mageia-cauldron
#: debian-bullseye archlinux fedora-rawhide opensuse-leap-15-5
#, no-wrap
msgid "August 2022"
msgstr "augusti 2022"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: mageia-cauldron archlinux opensuse-tumbleweed
#, no-wrap
msgid "COLOPHON"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#: debian-bullseye opensuse-leap-15-5 archlinux debian-unstable fedora-37
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#: archlinux
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller senare E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide opensuse-leap-15-4
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVNING"

#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed debian-bullseye debian-unstable
msgid "Display help text and exit."
msgstr ""

#: opensuse-leap-15-5 opensuse-tumbleweed debian-bullseye archlinux
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "Display version information and exit."
msgstr ""

#: debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed archlinux fedora-37 fedora-rawhide
#, no-wrap
msgid "ENVIRONMENT"
msgstr "MILJÖ"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "EXAMPLE"
msgstr "EXEMPEL"

#: debian-bullseye debian-unstable mageia-cauldron archlinux fedora-37
#: fedora-rawhide opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "EXEMPEL"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "SLUTSTATUS"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed archlinux mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "FILES"
msgstr "FILER"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed debian-unstable
msgid "For bug reports, use the issue tracker at"
msgstr ""

#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#: archlinux
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"GNU coreutils hjälp på nätet: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "LIBRARY"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#: archlinux
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide opensuse-leap-15-4
#, no-wrap
msgid "NAME"
msgstr "NAMN"

#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 debian-bullseye opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr "FLAGGOR"

#: opensuse-leap-15-5 fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr "oktober 2021"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#: archlinux debian-bullseye debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide mageia-cauldron
msgid "Print version and exit."
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTERA FEL"

#: debian-bullseye debian-unstable opensuse-leap-15-5 opensuse-tumbleweed
#: archlinux mageia-cauldron fedora-37 fedora-rawhide
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapportera anmärkningar på översättningen till E<lt>tp-sv@listor.tp-sv."
"seE<gt>."

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide opensuse-leap-15-4
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OCKSÅ"

#: debian-bullseye debian-unstable archlinux fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "september 2020"

#: debian-unstable fedora-rawhide fedora-37
#, no-wrap
msgid "September 2022"
msgstr "september 2022"

#: archlinux
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Detta är fri programvara: du får fritt ändra och vidaredistribuera den. Det "
"finns INGEN GARANTI, så långt lagen tillåter."

#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron archlinux
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr "Användarkommandon"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Värde"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid "display this help and exit"
msgstr "Visa denna hjälp och avsluta."

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid "output version information and exit"
msgstr "Visa versionsinformation och avsluta."

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "ge dig tillgång till den kompletta manualen."

#: opensuse-leap-15-5
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"

#: archlinux debian-unstable fedora-37 mageia-cauldron debian-bullseye
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 251"
msgstr "systemd 251"

#: fedora-rawhide
#, no-wrap
msgid "systemd 252"
msgstr "systemd 252"

#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.2"
msgstr "util-linux 2.37.2"

#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#: archlinux fedora-37 fedora-rawhide mageia-cauldron debian-unstable
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"
