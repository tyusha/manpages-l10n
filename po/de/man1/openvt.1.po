# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.16\n"
"POT-Creation-Date: 2022-10-03 15:45+0200\n"
"PO-Revision-Date: 2022-02-12 20:28+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.3\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPENVT"
msgstr "OPENVT"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "19 Jul 1996"
msgstr "19. Juli 1996"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "kbd"
msgstr "kbd"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "openvt - start a program on a new virtual terminal (VT)."
msgstr "openvt - ein Programm in einem neuen virtuellen Terminal (VT) starten."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<openvt> [-c vtnumber] [OPTIONS] [--] command"
msgstr "B<openvt> [-c VT-Nummer] [OPTIONEN] [--] Befehl"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<openvt> will find the first available VT, and run on it the given "
"B<command> with the given B<command options ,> standard input, output and "
"error are directed to that terminal. The current search path ($PATH) is used "
"to find the requested command. If no command is specified then the "
"environment variable $SHELL is used."
msgstr ""
"B<openvt> findet das erste verfügbare virtuelle Terminal (VT) und führt "
"darauf den angegebenen B<Befehl> mit den angegebenen B<OPTIONEN> aus, wobei "
"Standardeingabe, Standardausgabe und Standardfehlerausgabe an dieses "
"Terminal geleitet werden. Der aktuelle Suchpfad ($PATH) wird zum Finden des "
"angeforderten Befehls verwendet. Wird kein Befehl angegeben, wird die "
"Umgebungsvariable $SHELL verwendet."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-c, --console=VTNUMBER>"
msgstr "I<-c, --console=VT-NUMMER>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Use the given VT number and not the first available. Note you must have "
"write access to the supplied VT for this to work."
msgstr ""
"verwendet die angegebene VT-Nummer anstelle des ersten verfügbaren. Beachten "
"Sie, dass Sie über Schreibzugriff auf das angegebene VT verfügen müssen, "
"damit dies funktioniert."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-f, --force>"
msgstr "I<-f, --force>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Force opening a VT without checking whether it is already in use."
msgstr ""
"erzwingt das Öffnen eines VT, ohne zu prüfen, ob es bereits verwendet wird."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-e, --exec>"
msgstr "I<-e, --exec>"

#. type: Plain text
#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Directly execute the given command, without forking.  This option is meant "
"for use in I</etc/inittab>."
msgstr ""
"führt den angegebenen Befehl direkt aus, ohne es mit fork() zu starten. "
"Diese Option ist für die Verwendung in I</etc/inittab> gedacht."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-s, --switch>"
msgstr "I<-s, --switch>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Switch to the new VT when starting the command. The VT of the new command "
"will be made the new current VT."
msgstr ""
"wechselt beim Starten des Befehls zum neuen VT. Das VT des neuen Befehls "
"wird zum neuen aktuellen VT."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-u, --user>"
msgstr "I<-u, --user>"

# FIXME login → B<login>(1)
# FIXME Formatting of init
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Figure out the owner of the current VT, and run login as that user.  "
"Suitable to be called by init. Shouldn't be used with I<-c> or I<-l>."
msgstr ""
"ermittelt den Benutzer des aktuellen VTs und führt B<login>(1) als dieser "
"Benutzer aus. Dies ist zum Aufruf durch B<init> geeignet. Es sollte nicht "
"mit B<-c> oder B<-l> verwendet werden."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-l, --login>"
msgstr "I<-l, --login>"

# FIXME Formatting of the minus sign
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Make the command a login shell. A - is prepended to the name of the command "
"to be executed."
msgstr ""
"macht den Befehl zur Anmeldeshell. Dem auszuführenden Befehl wird ein B<-> "
"vorangestellt."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-v, --verbose>"
msgstr "I<-v, --verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Be a bit more verbose."
msgstr "schreibt ausführlichere Ausgaben."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-w, --wait>"
msgstr "I<-w, --wait>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"wait for command to complete. If -w and -s are used together then B<openvt> "
"will switch back to the controlling terminal when the command completes."
msgstr ""
"wartet auf den Abschluss der Ausführung des Befehls. Falls B<-w> und B<-s> "
"zusammen verwendet werden, wechselt B<openvt> zurück zum steuernden "
"Terminal, sobald die Befehlsausführung abgeschlossen ist."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-V, --version>"
msgstr "I<-V, --version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "print program version and exit."
msgstr "gibt die Programmversion aus und beendet das Programm."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-h, --help>"
msgstr "I<-h, --help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "show this text and exit."
msgstr "zeigt einen Hilfetext an und beendet das Programm."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<-->"
msgstr "I<-->"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "end of options to B<openvt>."
msgstr "markiert das Ende der Optionen für B<openvt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTE"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"If B<openvt> is compiled with a getopt_long() and you wish to set options to "
"the command to be run, then you must supply the end of options -- flag "
"before the command."
msgstr ""
"Falls B<openvt> mit getopt_long() kompiliert ist und Sie Optionen an den "
"auszuführenden Befehl übergeben wollen, dann müssen Sie vor den Befehl die "
"Ende-der-Optionen-Markierung B<--> setzen."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "BEISPIELE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"B<openvt> can be used to start a shell on the next free VT, by using the "
"command:"
msgstr ""
"B<openvt> kann mit folgendem Befehl zum Starten einer Shell auf dem nächsten "
"freien VT verwendet werden:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<openvt bash>"
msgstr "I<openvt bash>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "To start the shell as a login shell, use:"
msgstr "Um eine Shell als Anmeldeshell zu starten, geben Sie Folgendes ein:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<openvt -l bash>"
msgstr "I<openvt -l bash>"

# FIXME Formatting of the double minus sign
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "To get a long listing you must supply the -- separator:"
msgstr ""
"Um eine lange Auflistung zu erhalten, müssen Sie B<--> als Trenner verwenden:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "I<openvt -- ls -l>"
msgstr "I<openvt -- ls -l>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "HISTORY"
msgstr "GESCHICHTE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Earlier, B<openvt> was called B<open>.  It was written by Jon Tombs "
"E<lt>jon@gtex02.us.esE<gt> or E<lt>jon@robots.ox.ac.ukE<gt>.  The I<-w> idea "
"is from \"sam\"."
msgstr ""
"Frühere Versionen von B<openvt> hießen B<open>. Es wurde von Jon Tombs "
"E<lt>jon@gtex02.us.esE<gt> oder E<lt>jon@robots.ox.ac.ukE<gt> geschrieben. "
"Die Idee für B<-w> stammt von »sam«."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron
msgid "B<chvt>(1), B<doshell>(8), B<login>(1)"
msgstr "B<chvt>(1), B<doshell>(8), B<login>(1)"

# FIXME Formatting of -e
#. type: Plain text
#: debian-bullseye debian-unstable
msgid ""
"Directly execute the given command, without forking.  This option is meant "
"for use in I</etc/inittab>.  If you want to use this feature in another "
"context, be aware that B<openvt> has to be a session leader in order for -e "
"to work.  See B<setsid>(2)  or B<setsid>(1)  on how to achieve this."
msgstr ""
"führt den angegebenen Befehl direkt aus, ohne es mit fork() zu starten. "
"Diese Option ist für die Verwendung in I</etc/inittab> gedacht. Wenn Sie "
"diese Funktion in einem anderen Kontext verwenden wollen, achten Sie darauf, "
"dass B<openvt> der Sitzungsleiter sein muss, damit B<-e> funktioniert. In "
"B<setsid>(2) oder B<setsid>(1) finden Sie weitere Informationen, wie Sie "
"dies erreichen können."

#. type: Plain text
#: opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<chvt>(1), B<login>(1)"
msgstr "B<chvt>(1), B<login>(1)"
