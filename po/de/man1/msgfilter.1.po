# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.13\n"
"POT-Creation-Date: 2022-10-17 20:36+0200\n"
"PO-Revision-Date: 2022-03-24 12:00+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.12.3\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MSGFILTER"
msgstr "MSGFILTER"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "October 2022"
msgstr "Oktober 2022"

#. type: TH
#: archlinux mageia-cauldron
#, no-wrap
msgid "GNU gettext-tools 0.21.1"
msgstr "GNU gettext-tools 0.21.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "msgfilter - edit translations of message catalog"
msgstr "msgfilter - die Übersetzungen eines Meldungskatalogs bearbeiten"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<msgfilter> [I<\\,OPTION\\/>] I<\\,FILTER \\/>[I<\\,FILTER-OPTION\\/>]"
msgstr "B<msgfilter> [I<\\,OPTION\\/>] I<\\,FILTER \\/>[I<\\,FILTEROPTION\\/>]"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

# FIXME translation catalog → message catalog (for consistency with other gettext man pages)
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Applies a filter to all translations of a translation catalog."
msgstr "Wendet einen Filter auf alle Übersetzungen eines Meldungskatalogs an."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Die obligatorischen Argumente für Optionen sind für deren Kurz- und Langform "
"gleich."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Input file location:"
msgstr "Orte der Eingabedateien:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--input>=I<\\,INPUTFILE\\/>"
msgstr "B<-i>, B<--input>=I<\\,EINGABEDATEI\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "input PO file"
msgstr "PO-Eingabedatei"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-D>, B<--directory>=I<\\,DIRECTORY\\/>"
msgstr "B<-D>, B<--directory>=I<\\,VERZEICHNIS\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "add DIRECTORY to list for input files search"
msgstr ""
"fügt das angegebene VERZEICHNIS zur Liste hinzu, in der nach Eingabedateien "
"gesucht wird."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "If no input file is given or if it is -, standard input is read."
msgstr ""
"Wenn keine Eingabedatei oder »-« angegeben ist, wird die Standardeingabe "
"gelesen."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Output file location:"
msgstr "Orte der Ausgabedateien:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-o>, B<--output-file>=I<\\,FILE\\/>"
msgstr "B<-o>, B<--output-file>=I<\\,DATEI\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "write output to specified file"
msgstr "schreibt die Ausgabe in die angegebene DATEI."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The results are written to standard output if no output file is specified or "
"if it is -."
msgstr ""
"Die Ergebnisse werden in die Standardausgabe geschrieben, wenn keine Datei "
"oder »-« angegeben ist."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The FILTER can be any program that reads a translation from standard input "
"and writes a modified translation to standard output."
msgstr ""
"Der FILTER kann jedes Programm sein, das eine Übersetzung aus der "
"Standardeingabe liest und eine geänderte Übersetzung in die Standardausgabe "
"schreibt."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Filter input and output:"
msgstr "Filereingabe und -ausgabe:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--newline>"
msgstr "B<--newline>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"add a newline at the end of input and remove a newline from the end of output"
msgstr ""
"fügt einen Zeilenvorschub am Ende der Eingabe hinzu und entfernt einen "
"Zeilenvorschub vom Ende der Ausgabe."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Useful FILTER-OPTIONs when the FILTER is 'sed':"
msgstr "Nützliche FILTEROPTIONEN, wenn B<sed>(1) als FILTER verwendet wird:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-e>, B<--expression>=I<\\,SCRIPT\\/>"
msgstr "B<-e>, B<--expression>=I<\\,SKRIPT\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "add SCRIPT to the commands to be executed"
msgstr "fügt das angegebene SKRIPT zu den auszuführenden Befehlen hinzu."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--file>=I<\\,SCRIPTFILE\\/>"
msgstr "B<-f>, B<--file>=I<\\,SKRIPTDATEI\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "add the contents of SCRIPTFILE to the commands to be executed"
msgstr ""
"fügt den Inhalt der angegebenen SKRIPTDATEI zu den auszuführenden Befehlen "
"hinzu."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--quiet>, B<--silent>"
msgstr "B<-n>, B<--quiet>, B<--silent>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "suppress automatic printing of pattern space"
msgstr "unterdrückt die Ausgabe des Musterbereichs."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Input file syntax:"
msgstr "Syntax der Eingabedatei:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>, B<--properties-input>"
msgstr "B<-P>, B<--properties-input>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "input file is in Java .properties syntax"
msgstr "Eingabedatei folgt der I<.properties>-Syntax von Java."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--stringtable-input>"
msgstr "B<--stringtable-input>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "input file is in NeXTstep/GNUstep .strings syntax"
msgstr "Eingabedatei folgt der I<.strings>-Syntax von NeXTstep/GNUstep."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Output details:"
msgstr "Details zur Ausgabe:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--color>"
msgstr "B<--color>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use colors and other text attributes always"
msgstr "verwendet immer Farben und andere Text-Attribute."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--color>=I<\\,WHEN\\/>"
msgstr "B<--color>=I<\\,WERT\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"use colors and other text attributes if WHEN.  WHEN may be 'always', "
"'never', 'auto', or 'html'."
msgstr ""
"verwendet Farben und andere Text-Attribute. Der angegebene I<WERT> kann "
"»always«, »never«, »auto« oder »html« sein."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--style>=I<\\,STYLEFILE\\/>"
msgstr "B<--style>=I<\\,STILDATEI\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "specify CSS style rule file for B<--color>"
msgstr "gibt die CSS-Datei mit Stil-Regel für B<--color> an."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-escape>"
msgstr "B<--no-escape>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "do not use C escapes in output (default)"
msgstr "verwendet keine C-Maskier-Sequenzen in der Ausgabe (Vorgabe)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-E>, B<--escape>"
msgstr "B<-E>, B<--escape>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "use C escapes in output, no extended chars"
msgstr ""
"verwendet C-Maskier-Sequenzen in der Ausgabe, keine erweiterten Zeichen."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--force-po>"
msgstr "B<--force-po>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "write PO file even if empty"
msgstr "erstellt die PO-Datei auch dann, wenn sie leer ist."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--indent>"
msgstr "B<--indent>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "indented output style"
msgstr "stellt die Ausgabe eingerückt dar."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--keep-header>"
msgstr "B<--keep-header>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "keep header entry unmodified, don't filter it"
msgstr "belässt den Kopfeintrag und filtert ihn nicht."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-location>"
msgstr "B<--no-location>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "suppress '#: filename:line' lines"
msgstr "schreibt keine Zeilen mit »#: Dateiname:Zeilennummer«."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--add-location>"
msgstr "B<-n>, B<--add-location>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "preserve '#: filename:line' lines (default)"
msgstr "schreibt Zeilen mit »#: Dateiname:Zeilennummer« (Vorgabe)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--strict>"
msgstr "B<--strict>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "strict Uniforum output style"
msgstr "schreibt die Ausgabe strikt im Uniforum-Stil."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--properties-output>"
msgstr "B<-p>, B<--properties-output>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "write out a Java .properties file"
msgstr "schreibt eine I<.properties>-Datei für Java."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--stringtable-output>"
msgstr "B<--stringtable-output>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "write out a NeXTstep/GNUstep .strings file"
msgstr "schreibt eine I<.strings>-Datei für NeXTstep/GNUstep."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--width>=I<\\,NUMBER\\/>"
msgstr "B<-w>, B<--width>=I<\\,ANZAHL\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "set output page width"
msgstr "legt die Breite der Ausgabe auf die angegebene ANZAHL Spalten fest."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-wrap>"
msgstr "B<--no-wrap>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"do not break long message lines, longer than the output page width, into "
"several lines"
msgstr ""
"bricht Zeilen in Meldungen, die länger als die Breite der Ausgabe sind, "
"nicht auf mehrere Zeilen um."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-s>, B<--sort-output>"
msgstr "B<-s>, B<--sort-output>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "generate sorted output"
msgstr "erstellt eine sortierte Ausgabe."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-F>, B<--sort-by-file>"
msgstr "B<-F>, B<--sort-by-file>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "sort output by file location"
msgstr "sortiert die Ausgabe gemäß Vorkommen in Dateien."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Informative output:"
msgstr "Informative Ausgabe:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "zeigt Hilfeinformationen an und beendet das Programm."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "gibt Versionsinformationen aus und beendet das Programm."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "Written by Bruno Haible."
msgstr "Geschrieben von Bruno Haible."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Report bugs in the bug tracker at E<lt>https://savannah.gnu.org/projects/"
"gettextE<gt> or by email to E<lt>bug-gettext@gnu.orgE<gt>."
msgstr ""
"Melden Sie Fehler im Fehlererfassungssystem auf E<lt>https://savannah.gnu."
"org/projects/gettextE<gt> oder per E-Mail an E<lt>bug-gettext@gnu.orgE<gt> "
"(jeweils auf Englisch)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux mageia-cauldron
msgid ""
"Copyright \\(co 2001-2022 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Copyright \\(co 2001-2022 Free Software Foundation, Inc. Lizenz GPLv3+: E<."
"UR https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"The full documentation for B<msgfilter> is maintained as a Texinfo manual.  "
"If the B<info> and B<msgfilter> programs are properly installed at your "
"site, the command"
msgstr ""
"Die vollständige Dokumentation für B<msgfilter> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<msgfilter> auf Ihrem "
"Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "B<info msgfilter>"
msgstr "B<info msgfilter>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "February 2021"
msgstr "Februar 2021"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide
#, no-wrap
msgid "GNU gettext-tools 0.21"
msgstr "GNU gettext-tools 0.21"

#. type: Plain text
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed
msgid ""
"Copyright \\(co 2001-2020 Free Software Foundation, Inc.  License GPLv3+: "
"GNU GPL version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>"
msgstr ""
"Copyright \\(co 2001-2020 Free Software Foundation, Inc. Lizenz GPLv3+: E<."
"UR https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: TH
#: debian-unstable
#, no-wrap
msgid "August 2022"
msgstr "August 2022"

#. type: TH
#: fedora-37 fedora-rawhide
#, no-wrap
msgid "March 2022"
msgstr "März 2022"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "April 2020"
msgstr "April 2020"

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "GNU gettext-tools 0.20.2"
msgstr "GNU gettext-tools 0.20.2"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "July 2020"
msgstr "Juli 2020"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "GNU gettext-tools 20200704"
msgstr "GNU gettext-tools 20200704"
