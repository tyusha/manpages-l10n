# Dutch translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Jos Boersema <joshb@xs4all.nl>, 2001.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019.
# Luc Castermans <luc.castermans@gmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 2.15\n"
"POT-Creation-Date: 2022-10-17 21:05+0200\n"
"PO-Revision-Date: 2021-12-10 20:03+0100\n"
"Last-Translator: Luc Castermans <luc.castermans@gmail.com>\n"
"Language-Team: Dutch <debian-l10n-german@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.08.0\n"

#: debian-bullseye debian-unstable archlinux fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed mageia-cauldron
#, no-wrap
msgid "*"
msgstr "*"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTEN"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed mageia-cauldron archlinux opensuse-leap-15-4
#, no-wrap
msgid "AUTHOR"
msgstr "AUTEUR"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr "AUTEURS"

#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed debian-bullseye debian-unstable
#, no-wrap
msgid "AVAILABILITY"
msgstr "BESCHIKBAARHEID"

#: debian-unstable mageia-cauldron archlinux fedora-37 fedora-rawhide
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribuut"

#: fedora-37 opensuse-tumbleweed debian-unstable mageia-cauldron
#: debian-bullseye archlinux fedora-rawhide opensuse-leap-15-5
#, no-wrap
msgid "August 2022"
msgstr "Augustus 2022"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr "B<EFAULT>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr "B<ENOMEM>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr "B<EPERM>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BUGS"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: mageia-cauldron archlinux opensuse-tumbleweed
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFON"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "VOLDOET AAN"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#: debian-bullseye opensuse-leap-15-5 archlinux debian-unstable fedora-37
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#: archlinux
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  Licentie GPLv3+: GNU "
"GPL versie 3 of later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide opensuse-leap-15-4
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHRIJVING"

#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed debian-bullseye debian-unstable
msgid "Display help text and exit."
msgstr "een hulptekst tonen en stoppen."

#: opensuse-leap-15-5 opensuse-tumbleweed debian-bullseye archlinux
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid "Display version information and exit."
msgstr "programmaversie tonen en stoppen."

#: debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed archlinux fedora-37 fedora-rawhide
#, no-wrap
msgid "ENVIRONMENT"
msgstr "OMGEVING"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FOUTEN"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "EXAMPLE"
msgstr "VOORBEELD"

#: debian-bullseye debian-unstable mageia-cauldron archlinux fedora-37
#: fedora-rawhide opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr "VOORBEELDEN"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr "EIND WAARDE"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide opensuse-leap-15-5
#: opensuse-tumbleweed archlinux mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "FILES"
msgstr "BESTANDEN"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr "Feature Test Macro´s eisen in  glibc (zie B<feature_test_macros>(7)):"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr "Voor een uitleg van de termen in deze sectie, zie B<attributes>(7)."

#: archlinux fedora-37 fedora-rawhide mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed debian-unstable
msgid "For bug reports, use the issue tracker at"
msgstr "Gebruik om bugs te rapporteren de issue tracker op"

#: debian-bullseye opensuse-leap-15-5
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron opensuse-tumbleweed
#: archlinux
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Online hulp bij GNU coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "LIBRARY"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux Programmeurs Handleiding"

#: archlinux
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide opensuse-leap-15-4
#, no-wrap
msgid "NAME"
msgstr "NAAM"

#: archlinux debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 debian-bullseye opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "OPMERKINGEN"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIES"

#: opensuse-leap-15-5 fedora-37 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr "Oktober 2021"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: opensuse-leap-15-5 opensuse-tumbleweed mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#: archlinux debian-bullseye debian-unstable opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide mageia-cauldron
msgid "Print version and exit."
msgstr "Toon versie en stop."

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr "RAPPORTEREN VAN BUGS"

#: debian-bullseye debian-unstable opensuse-leap-15-5 opensuse-tumbleweed
#: archlinux mageia-cauldron fedora-37 fedora-rawhide
#, no-wrap
msgid "RETURN VALUE"
msgstr "EIND WAARDE"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Meld alle vertaalfouten op E<lt>https://translationproject.org/team/nl."
"htmlE<gt>"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide opensuse-leap-15-4
#, no-wrap
msgid "SEE ALSO"
msgstr "ZIE OOK"

#: debian-bullseye debian-unstable archlinux fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "STANDARDS"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr "SAMENVATTING"

#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "September 2020"

#: debian-unstable fedora-rawhide fedora-37
#, no-wrap
msgid "September 2022"
msgstr "September 2022"

#: archlinux
msgid "Standard C library (I<libc>, I<-lc>)"
msgstr ""

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dit is vrije software: u mag het vrijelijk wijzigen en verder verspreiden. "
"Deze software kent GEEN GARANTIE, voor zover de wet dit toestaat."

#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 4.16 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 5.10 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron archlinux
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Deze pagina is onderdeel van release 5.13 van het Linux I<man-pages>-"
"project. Een beschrijving van het project, informatie over het melden van "
"bugs en de nieuwste versie van deze pagina zijn op \\%https://www.kernel.org/"
"doc/man-pages/ te vinden."

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread veiligheid"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr "Opdrachten voor gebruikers"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIES"

#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Waarde"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid "display this help and exit"
msgstr "toon de helptekst en stop"

#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed archlinux
msgid "output version information and exit"
msgstr "toon programmaversie en stop"

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-5
#: opensuse-tumbleweed fedora-37 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr "toegang tot de volledige handleiding."

#: opensuse-leap-15-5
#, no-wrap
msgid "systemd 249"
msgstr "systemd 249"

#: archlinux debian-unstable fedora-37 mageia-cauldron debian-bullseye
#: opensuse-tumbleweed
#, no-wrap
msgid "systemd 251"
msgstr "systemd 251"

#: fedora-rawhide
#, fuzzy, no-wrap
#| msgid "systemd 251"
msgid "systemd 252"
msgstr "systemd 251"

#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#: opensuse-leap-15-5
#, no-wrap
msgid "util-linux 2.37.2"
msgstr "util-linux 2.37.2"

#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"

#: archlinux fedora-37 fedora-rawhide mageia-cauldron debian-unstable
#, no-wrap
msgid "util-linux 2.38.1"
msgstr "util-linux 2.38.1"
