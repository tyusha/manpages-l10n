# Italian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Giulio Daprelà <giulio@pluto.it>, 2005, 2006.
# Marco Curreli <marcocurreli@tiscali.it>, 2013, 2018, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2022-10-17 20:29+0200\n"
"PO-Revision-Date: 2021-01-12 23:23+0100\n"
"Last-Translator: Marco Curreli <marcocurreli@tiscali.it>\n"
"Language-Team: Italian <pluto-ildp@lists.pluto.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "INTRO"
msgstr "INTRO"

#. type: TH
#: archlinux
#, no-wrap
msgid "2022-09-09"
msgstr "9 settembre 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "Linux man-pages (unreleased)"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid "intro - introduction to library functions"
msgstr "intro - Introduzione alle funzioni di libreria"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Section 3 of the manual describes all library functions excluding the "
"library functions (system call wrappers) described in Section 2, which "
"implement system calls."
msgstr ""
"La Sezione 3 del manuale descrive tutte le funzioni di libreria, escluse le "
"funzioni di libreria descritte nella sezione 2, che implementano le chiamate "
"di sistema."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Many of the functions described in the section are part of the Standard C "
"Library (I<libc>).  Some functions are part of other libraries (e.g., the "
"math library, I<libm>, or the real-time library, I<librt>)  in which case "
"the manual page will indicate the linker option needed to link against the "
"required library (e.g., I<-lm> and I<-lrt>, respectively, for the "
"aforementioned libraries)."
msgstr ""
"Molte delle funzioni descritte nella sezione appartengono alla Libreria C "
"Standard (I<libc>). Alcune funzioni fanno parte di altre librerie (per "
"esempio la libreria matematica, I<libm>, o la libreria real-time I<librt>) "
"nel qual caso la pagina di manuale indicherà l'opzione del linker necessaria "
"a collegare la libreria richiesta (ad esempio, I<-lm> e I<-lrt>, "
"rispettivamente, per librerie precedentemente citate)."

#.  There
#.  are various function groups which can be identified by a letter which
#.  is appended to the chapter number:
#.  .IP (3C)
#.  These functions, the functions from chapter 2 and from chapter 3S are
#.  contained in the C standard library libc, which will be used by
#.  .BR cc (1)
#.  by default.
#.  .IP (3S)
#.  These functions are parts of the
#.  .BR stdio (3)
#.  library.  They are contained in the standard C library libc.
#.  .IP (3M)
#.  These functions are contained in the arithmetic library libm.  They are
#.  used by the
#.  .BR f77 (1)
#.  FORTRAN compiler by default, but not by the
#.  .BR cc (1)
#.  C compiler, which needs the option \fI\-lm\fP.
#.  .IP (3F)
#.  These functions are part of the FORTRAN library libF77.  There are no
#.  special compiler flags needed to use these functions.
#.  .IP (3X)
#.  Various special libraries.  The manual pages documenting their functions
#.  specify the library names.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"In some cases, the programmer must define a feature test macro in order to "
"obtain the declaration of a function from the header file specified in the "
"man page SYNOPSIS section.  (Where required, these feature test macros must "
"be defined before including I<any> header files.)  In such cases, the "
"required macro is described in the man page.  For further information on "
"feature test macros, see B<feature_test_macros>(7)."
msgstr ""
"In alcuni casi il programmatore deve definire una macro di test per ottenere "
"la dichiarazione di una funzione dal file di intestazione specificato nella "
"sezione SINTASSI della pagina di manuale. (Dove richieste, queste macro con "
"funzionalità di test devono essere definite prima di includere I<qualsiasi> "
"file di intestazione). In tali casi, la macro richiesta è descritta nella "
"pagina di manuale. Per ulteriori informazioni sulle macro di test, vedere "
"B<feature_test_macros>(7)."

#. type: SH
#: archlinux
#, no-wrap
msgid "STANDARDS"
msgstr "STANDARDS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Certain terms and abbreviations are used to indicate UNIX variants and "
"standards to which calls in this section conform.  See B<standards>(7)."
msgstr ""
"Alcuni termini e abbreviazioni sono usati per indicare varianti UNIX e "
"standard  ai quali si conformano le chiamate in questa sezione. See "
"B<standards>(7)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTE"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Authors and copyright conditions"
msgstr "Autori e condizioni di copyright"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
msgid ""
"Look at the header of the manual page source for the author(s) and copyright "
"conditions.  Note that these can be different from page to page!"
msgstr ""
"Si vedano le intestazioni delle pagine di manuale per l'autore(i) e le "
"condizioni di copyright. Si noti che questi possono differire da pagina a "
"pagina!"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-37 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7), "
"B<system_data_types>(7)"
msgstr ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7), "
"B<system_data_types>(7)"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2020-11-01"
msgstr "1 novembre 2020"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuale del programmatore di Linux"

#. type: SH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORME A"

#. type: SH
#: debian-bullseye debian-unstable fedora-37 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-5
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Questa pagina fa parte del rilascio 5.10 del progetto Linux I<man-pages>. "
"Una descrizione del progetto, le istruzioni per la segnalazione degli "
"errori, e l'ultima versione di questa pagina si trovano su \\%https://www."
"kernel.org/doc/man-pages/."

#. type: Plain text
#: debian-unstable fedora-37 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Questa pagina fa parte del rilascio 5.13 del progetto Linux I<man-pages>. "
"Una descrizione del progetto, le istruzioni per la segnalazione degli "
"errori, e l'ultima versione di questa pagina si trovano su \\%https://www."
"kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-5
#, no-wrap
msgid "2017-09-15"
msgstr "15 settembre 2017"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7)"
msgstr ""
"B<intro>(2), B<errno>(3), B<capabilities>(7), B<credentials>(7), "
"B<environ>(7), B<feature_test_macros>(7), B<libc>(7), B<math_error>(7), "
"B<path_resolution>(7), B<pthreads>(7), B<signal>(7), B<standards>(7)"

#. type: Plain text
#: opensuse-leap-15-5
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Questa pagina fa parte del rilascio 4.16 del progetto Linux I<man-pages>. "
"Una descrizione del progetto, le istruzioni per la segnalazione degli "
"errori, e l'ultima versione di questa pagina si trovano su \\%https://www."
"kernel.org/doc/man-pages/."
